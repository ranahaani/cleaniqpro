//
//  CustomTextField.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 01/08/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import Foundation
import UIKit

class CustomTextField: UITextField {
    
    let sections: [String] = ["Booking urn On/OffT", "Patient Limit", "General Settings"]
    let s1Data: [String] = ["Season 1", "Season 2", "Season 3"]
    let s2Data: [String] = ["Total Patient in S1", "Total Patient in S2", "Total Patient in S3"]
    let s3Data: [String] = ["FAQ's", "Booking Records & Stats", "Broadcast Message", "Notifications", "My Requests","Invite"]
    var sectionData: [Int: [String]] = [:]
//     sectionData = [0:s1Data, 1:s2Data, 2:s3Data]
}
