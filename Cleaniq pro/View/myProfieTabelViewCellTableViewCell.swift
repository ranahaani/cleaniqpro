//
//  myProfieTabelViewCellTableViewCell.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 31/07/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit

class myProfieTabelViewCellTableViewCell: UITableViewCell {
    @IBOutlet weak var placeHolderLabel: UILabel!
    @IBOutlet weak var TextLabel: UILabel!
    var delegate: TextFieldInTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    @IBAction func editIconToShowPopup(_ sender: Any) {
        didSelectCell()
    }
    
}

// MARK: - Actions

extension myProfieTabelViewCellTableViewCell {
    
    @objc func didSelectCell() {
        TextLabel.becomeFirstResponder()
        delegate?.textFieldInTableViewCell(didSelect: self)
    }

}
