//
//  FAQTableViewCell.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 30/09/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit

class FAQTableViewCell: UITableViewCell {

    @IBOutlet weak var answer: UILabel!
    @IBOutlet weak var question: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
