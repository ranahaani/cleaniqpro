
//
//  patientListTableViewCell.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 21/09/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit

class patientListTableViewCell: UITableViewCell {

    @IBOutlet weak var tokenNumber: UILabel!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var customerAge: UILabel!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var customerPhone: UILabel!
    @IBOutlet weak var customImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cellView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        cellView.layer.borderWidth = 1
        Name.font = UIFont(name: "Nunito-Black", size: 17)
        Name.textAlignment = .center
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
