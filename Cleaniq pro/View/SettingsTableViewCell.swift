//
//  SettingsTableViewCell.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 03/08/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
class SettingsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cell4Button: UIButton!
    @IBOutlet weak var noOfPatient: UILabel!
    @IBOutlet weak var cell2TextField: UITextField!
    @IBOutlet weak var edditButton: UIButton!
    @IBOutlet weak var switchButton: UISwitch!
    @IBOutlet weak var CellTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var isTextFieldEdit:Bool = false
    
    @IBAction func clrearDuesPressed(_ sender: Any) {
    }
    @IBAction func editButtonPressed(_ sender: Any) {
        if !isTextFieldEdit{
            edditButton.setTitle("Done", for: .normal)
            //edditButton.setImage(#imageLiteral(resourceName: "icons8-checkmark-25"), for: .normal)
            cell2TextField.isHidden = false
            noOfPatient.isHidden = true
             cell2TextField.becomeFirstResponder()
            isTextFieldEdit = true
        }
        else{
            
            //edditButton.setImage(#imageLiteral(resourceName: "edit-draw-pencil-1"), for: .normal)
            edditButton.setTitle("Edit", for: .normal)
            cell2TextField.isHidden = true
            noOfPatient.isHidden = false
            cell2TextField.endEditing(true)
            isTextFieldEdit = false
        }
        print(cell2TextField.text ?? "Haani")
        noOfPatient.text = cell2TextField.text
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
