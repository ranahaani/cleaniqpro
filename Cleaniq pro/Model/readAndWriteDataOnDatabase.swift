//
//  readAndWriteDataOnDatabase.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 06/08/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import Foundation
import FirebaseFirestore
import FirebaseAuth
class readAndWriteDataOnDatabase {
    let db = Firestore.firestore()
    var CellNameLabel = ["Name", "Clinic","Status", "Tiltle", "Address", "Contact No", "", "Plan Validity"]{
        didSet{
            
        }
    }
    var shiftNumber = "0"
    var reasonForShiftChange = ""
    var shiftNumberToChange = "0"
    var TimeStamp = Date()
    
    
    func getCellNameLabel()->String{
        return CellNameLabel[0]
    }
    
    
    func retriveData(){
        if let userID = Auth.auth().currentUser?.uid {
            db.collection("DoctorCity").document(userID)
                .getDocument { (snapshot, error ) in
                    
                    if let document = snapshot {
                        let city = document.get("city")
                        self.db.collection("Doctors").document(city as! String).collection("DoctorsList").document(userID).getDocument(completion: { (snapshota, error) in
                            if let fDocoment = snapshota {
                                //final document
                                print(fDocoment.get("doctorName") ?? "Name")
                                self.CellNameLabel[0].removeAll()
                                self.CellNameLabel[0].append(fDocoment.get("doctorName") as! String)
                                self.CellNameLabel[1].removeAll()
                                self.CellNameLabel[1].append(fDocoment.get("drClinicName") as! String)
                                self.CellNameLabel[2].removeAll()
                                self.CellNameLabel[2].append(fDocoment.get("doctorStatus") as! String)
                                self.CellNameLabel[3].removeAll()
                                self.CellNameLabel[3].append(fDocoment.get("doctorTitle") as! String)
                                self.CellNameLabel[4].removeAll()
                                self.CellNameLabel[4].append(fDocoment.get("drAddress") as! String)
                                self.CellNameLabel[5].removeAll()
                                let contactNo = fDocoment.get("contactNo")!
                                print(String(describing: contactNo))
                                self.CellNameLabel[5].append(String(describing: contactNo))
                                self.TimeStamp = fDocoment.get("planValidTill") as! Date
                                print(self.TimeStamp)
                                self.CellNameLabel[7].removeAll()
                                self.CellNameLabel[7].append(String(describing: self.TimeStamp))
                                self.CellNameLabel[7].removeLast(6)
                                
                            }
                            else{
                                print("There's no data against current doctor")
                            }
                        })
                        
                        
                    } else {
                        
                        print("Document does not exist")
                    }
            }
        }
    }
}

