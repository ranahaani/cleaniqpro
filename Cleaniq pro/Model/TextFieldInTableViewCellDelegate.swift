//
//  TextFieldInTableViewCellDelegate.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 31/07/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import Foundation
import UIKit

protocol TextFieldInTableViewCellDelegate {
    func textFieldInTableViewCell(didSelect cell:myProfieTabelViewCellTableViewCell)
    func textFieldInTableViewCell(cell:myProfieTabelViewCellTableViewCell, editingChangedInTextField newText:String)
}
