//
//  AppDelegate.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 28/07/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import SVProgressHUD
import Reachability
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow? 
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        FirebaseApp.configure()
        let reachability = Reachability()

        self.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if (reachability?.connection == .wifi) || (reachability?.connection == .cellular){
            let db = Firestore.firestore()
            let settings = db.settings
            settings.areTimestampsInSnapshotsEnabled = true
            db.settings = settings
            SVProgressHUD.showInfo(withStatus: reachability?.connection.description)
        if (Auth.auth().currentUser?.uid) != nil {
            let initialViewController: LunchScreenViewController = mainStoryboard.instantiateViewController(withIdentifier: "toLunchScreen") as! LunchScreenViewController
            self.window?.rootViewController = initialViewController
        }
        else{
            let initialViewController: LoginVC = mainStoryboard.instantiateViewController(withIdentifier: "Login") as! LoginVC
            self.window?.rootViewController = initialViewController
        
            }
        }
        else{



            let alert = UIAlertController(title: "", message: "", preferredStyle: .actionSheet)
            //alert.showToast(message: "check your internet connection")
            alert.view.backgroundColor = UIColor.white
            let imageView = UIImageView(frame: CGRect(alert.view.frame.width/2 - 150, alert.view.frame.height/2 - 300, 300, 600))
            imageView.image = #imageLiteral(resourceName: "ss")
            imageView.contentMode = .scaleAspectFit
            alert.view.addSubview(imageView)
            let LabelView = UILabel(frame: CGRect(alert.view.frame.width/2 - 100, alert.view.frame.height/2 + 210, 200, 50))
            LabelView.text = reachability?.connection.description
            LabelView.textAlignment = .center
            alert.view.addSubview(LabelView)
            SVProgressHUD.setOffsetFromCenter(UIOffsetMake(0, 110))
            SVProgressHUD.showError(withStatus: reachability?.connection.description)
            self.window?.rootViewController = alert
        }
        self.window?.makeKeyAndVisible()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

