//
//  AddPatientViewController.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 29/08/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import SVProgressHUD
class AddPatientViewController: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    @IBOutlet weak var confirmAppointment: UIButton!
    @IBOutlet weak var genderTextField: UITextField!
    @IBOutlet weak var dropDown: UIPickerView!
    @IBOutlet weak var patientName: UITextField!
    @IBOutlet weak var patientPhone: UITextField!
    @IBOutlet weak var patientAge: UITextField!
    let db = Firestore.firestore()
    var refArray = [DocumentReference]()
    let dateString: String = {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let Finaldate = dateFormatter.string(from: date)
        return Finaldate
    }()
    var list = ["Men", "Women", "Other"]
    var SessionNumber:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "cleaniqText.png")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        dropDown.isHidden = true
        genderTextField.isUserInteractionEnabled = false;
        genderTextField.borderStyle = .none
        genderTextField.layer.borderWidth = 0
        view.layer.backgroundColor = UIColor(red: 0.97, green: 0.97, blue: 0.97, alpha: 1).cgColor
        //setupViews()
    }
    func setupViews(){
        //PateintNameLabel.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        //PateintNameLabel.bottomAnchor.constraint(equalTo: patientName.topAnchor, constant: 11).isActive = true

        //patientName.translatesAutoresizingMaskIntoConstraints = false
        //patientName.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        //patientName.topAnchor.constraint(equalTo: PateintNameLabel.bottomAnchor, constant: 16).isActive = true
    }
    
    @IBAction func confirmBookingPressed(_ sender: Any) {
        var tokenVal:Int = 0
        var nn:Int = 0
        var city:String!
        if let userId = Auth.auth().currentUser?.uid{
            db.collection("DoctorCity").document(userId).getDocument(completion: { (snapShotOne, errOne) in
                 city = snapShotOne?.get("city") as? String
            self.db.collection("Doctors").document(city).collection("DoctorsList").document(userId).getDocument(completion: { (snapShotTwo, errTwo) in
                    if (snapShotTwo?.exists)!
                    {
                        nn = (snapShotTwo?.get("maxPatient\(self.SessionNumber!)") as? Int)!
                    }
                })
            })
            //print(patientName.text, patientPhone.text, patientAge.text!, genderTextField.text!)
            if patientPhone.text?.count == 10 {
                let alert = UIAlertController(title: "Appointment Confirmation", message: "Have you input all the details correctly?", preferredStyle: .alert)
                let saveAction = UIAlertAction(title: "Save",style: .default){
                    (action: UIAlertAction!) -> Void in
                    let sfDocRef = self.db.collection("Bookings").document(userId).collection("shift\(self.SessionNumber!)").document(self.dateString)
                    var mdata = Dictionary<String, Any>()
                    self.db.runTransaction({ (transaction, errorPointer) -> Any? in
                        let snapshot: DocumentSnapshot
                        do {
                            try snapshot = transaction.getDocument(sfDocRef)
                             tokenVal = snapshot.get("tokens") as! Int + 1
                            let bookingstarted = snapshot.get("bookingStarted") as! Bool
                            mdata["tokens"] = tokenVal;
                            mdata["timestamp"] = FieldValue.serverTimestamp()
                            mdata["patientName"] = self.patientName.text
                            mdata["patientFee"] = 0
                            mdata["patientPNumber"] = self.patientPhone.text
                            mdata["patientNote"] = " "
                            mdata["patientGender"] = self.genderTextField.text
                            mdata["patientAge"] = self.patientAge.text
                            mdata["patientStatus"] = "Booked"

                            if (bookingstarted==true)
                            {
                                if (tokenVal<=nn)
                                {
                                    transaction.updateData( ["tokens": tokenVal], forDocument: sfDocRef)
                                    transaction.setData(mdata, forDocument: sfDocRef.collection("appointments").document(self.patientPhone.text!))
                                }
                                else
                                {
                                    SVProgressHUD.showInfo(withStatus: "Queue Full")
                                }
                            }
                            else
                            { }
                        } catch let fetchError as NSError {
                            print(fetchError.localizedDescription)
                            errorPointer?.pointee = fetchError
                            return nil
                        }

                        return nil
                    }) { (object, error) in
                        if let error = error {
                            print("\(error.localizedDescription), 1 number")
                        } else {
                            print("Transaction successfully committed!")
                            self.patientName.text = ""
                            self.patientPhone.text = ""
                            self.patientAge.text = ""
                            SVProgressHUD.showInfo(withStatus:  "Appointmnet Booked!")
                            let documentReferenceAdd = self.db.collection("Doctors").document(city).collection("DoctorsList").document(userId)
                            self.db.collection("Global").document("variables").getDocument(completion: { (snapShotThree, err) in
                                let patientAmount = snapShotThree?.get("perPatientAmount") as! Double


                                self.db.runTransaction({ (transactionOne, errorPointerOne) -> Any? in
                                    let sfDocumentOne: DocumentSnapshot
                                    do {
                                        try sfDocumentOne = transactionOne.getDocument(documentReferenceAdd)
                                         let updateBalance = sfDocumentOne.get("myBalance") as! Double
                                        let x = updateBalance - patientAmount
                                        transactionOne.updateData(["myBalance":x], forDocument: documentReferenceAdd)

                                    } catch let fetchError as NSError {
                                        errorPointerOne?.pointee = fetchError
                                        return nil
                                    }

                                    return nil
                                }) { (object, error) in
                                    if let error = error {
                                        print("\(error.localizedDescription)", "2 number")
                                        SVProgressHUD.showError(withStatus: error.localizedDescription)
                                    } else {
                                        print("Transaction successfully committed!")
                                        self.sendSmsToUser(tokenVal: tokenVal, patientName: self.patientName.text!, patientPhone: self.patientPhone.text!)
                                        let alert = UIAlertController(title: "Successful", message: "Thanks your Appointment is booked.Your Token is \n ->  \(tokenVal)", preferredStyle: .alert)
                                        let action = UIAlertAction(title: "OK", style: .default, handler: { (_) in

                                        })
                                        alert.addAction(action)
                                        self.present(alert, animated: true, completion: nil)
                                    }
                                }
                                
                            })
                        }
                    }
                }
                let cancelAction = UIAlertAction(title: "Cancel",style: .default) { (action: UIAlertAction!) -> Void in}

                alert.addAction(saveAction)
                alert.addAction(cancelAction)
                present(alert,animated: true,completion: nil)
            }
            let alert = UIAlertController(title: "Error", message: "Please Enter Phone Number Correctly", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok",style: .default) { (action: UIAlertAction!) -> Void in}
            alert.addAction(cancelAction)
            present(alert,animated: true,completion: nil)
        }

    }
    
    func sendSmsToUser(tokenVal: Int, patientName: String, patientPhone:String){

    }
    @IBAction func GenderPressed(_ sender: Any) {
        dropDown.isHidden = false
        confirmAppointment.isHidden = true
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
        
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        return list.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        self.view.endEditing(true)
        return list[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.genderTextField.text = self.list[row]
        self.dropDown.isHidden = true
        confirmAppointment.isHidden = false
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.genderTextField {
            self.dropDown.isHidden = false            
            textField.endEditing(true)
        }
        else{
            self.dropDown.isHidden = true
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
