//
//  LunchScreenViewController.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 04/08/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit
import FirebaseFirestore
import Reachability
import FirebaseAuth
import SVProgressHUD
class LunchScreenViewController: UIViewController {
    let reachability = Reachability()
    let db = Firestore.firestore()
    var refArray = [DocumentReference]()
    let dateString: String = {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let Finaldate = dateFormatter.string(from: date)
        return Finaldate
    }()
    var currDate:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.setOffsetFromCenter(UIOffsetMake(0, 210))
        SVProgressHUD.show()
        if (reachability?.connection == .wifi) || (reachability?.connection == .cellular){
            print(reachability?.connection ?? "Connection established")
            self.isShiftsCreated(completionHandler: { (result ) in
                if result{
                    //Fetching Doctor information
                    if let userID = Auth.auth().currentUser?.uid {
                        let docRef = self.db.collection("DoctorCity").document(userID)
                        docRef.getDocument(completion: { (snapshot, err) in
                            let city = snapshot?.get("city")
                            let dRef = self.db.collection("Doctors").document(city as! String).collection("DoctorsList").document(userID)
                            dRef.getDocument(completion: { (docSnapshot, err) in
                                if let timestamp: Timestamp = docSnapshot?.get("joindate") as? Timestamp{
                                    let JoinDate: Date = timestamp.dateValue()
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "dd-MM-yyyy"
                                    
                                    if let currentDate = dateFormatter.date(from: self.dateString){
                                        if JoinDate < currentDate {
                                            let numberOfDays = self.daysBetweenDates(startDate: JoinDate,endDate: currentDate)
                                            var months = numberOfDays - (numberOfDays%30);
                                            months = months/30
                                            for i in stride(from: 1, through: months, by: 1){

                                                self.TransactionData(dRef: dRef, i: i, completionHandler: { (isDone) in
                                                    if isDone{
                                                        print("Transaction Successful")
                                                    }
                                                    else{
                                                        print("Transaction failed: \(i)")
                                                    }
                                                })
                                            }
                                            self.performSegue(withIdentifier: "toTabBarVC", sender: nil)
                                            SVProgressHUD.dismiss()
                                        }
                                    }
                                }
                            })
                        })
                    }
                }
                else{
                    self.createDocument(current: self.dateString)
                    SVProgressHUD.show(withStatus: "Creating Shift...")
                    Timer.scheduledTimer(timeInterval: 10.0, target: self, selector: #selector(self.wait5Sec), userInfo: nil, repeats: false)

                }
            })
        }
        else {
            SVProgressHUD.dismiss()
            alerController(set: "check your connection", set: (reachability?.connection.description)!)
            print(reachability?.connection ?? "error", "error")
        }
    }
    @objc func wait5Sec(){
        SVProgressHUD.dismiss()
        self.performSegue(withIdentifier: "toTabBarVC", sender: nil)
    }
    func alerController(set alertMessage: String, set title:String){
        let alert = UIAlertController(title: title, message: alertMessage, preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "",style: .default)
        alert.addAction(saveAction)
        present(alert,animated: true,completion: nil)
    }
    func TransactionData(dRef: DocumentReference, i:Int,completionHandler: @escaping (Bool) -> Void){
        dRef.collection("payMonths").document(String(i)).getDocument(completion: { (NsnapShot, err) in
            let Mvalue = NsnapShot?.get("value") as? Int
            if Mvalue == 0 {
                self.db.collection("Global").document("variables").getDocument(completion: { (DocSnap, err) in
                    let cleaniqFee = DocSnap?.get("cleaniqFee") as? Double
                    var finalAmount: Double!
                    self.db.runTransaction({ (transaction, errorPointer) -> Any? in
                        let sfDocument: DocumentSnapshot
                        do {
                            try sfDocument = transaction.getDocument(dRef)
                            let amountToBeDeduct = sfDocument.get("myBalance") as? Double
                            finalAmount =  (Double(amountToBeDeduct!)) - (Double(cleaniqFee!))

                        } catch let fetchError as NSError {
                            errorPointer?.pointee = fetchError
                            return nil
                        }
                        transaction.updateData(["myBalance": finalAmount], forDocument: dRef)
                        dRef.collection("payMonths").document(String(i)).updateData(["value":1])
                        return nil
                    }) { (object, error) in
                        if let error = error {
                            print("\(error)")
                            completionHandler(false)
                        } else {
                            print("Transaction successfully committed!", i)
                            completionHandler(true)
                        }
                    }
                })
            }
            else if Mvalue == 1{
                print("Value is 1")
            }

        })

    }
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let daysBetween = Calendar.current.dateComponents([.day], from: startDate, to: endDate)
        return daysBetween.day!
    }
    func completeUploading(numberOfShifts: Int){
        
        let Batch: WriteBatch = db.batch()
        var BatchData = Dictionary<String, Any>()
        BatchData["liveNumber"] = 0
        BatchData["queueStatus"] = "closed"
        BatchData["tokens"] = 0
        BatchData["bookingStarted"] = false
        BatchData["completed"] = false

        for i in 0..<numberOfShifts{
            Batch.setData(BatchData, forDocument: refArray[i])
        }

        var NextSession = Dictionary<String, Any>()
        NextSession["nextSession"] = 1
        NextSession["allBookings"] = false
        NextSession["noOfSessionsCompleted"] = 0
        if let userID = Auth.auth().currentUser?.uid{
            Batch.setData(NextSession, forDocument: db.collection("Bookings").document(userID))
        }
        Batch.commit() { err in
            if let err = err {
                print("Error writing batch \(err)")
            } else {
                self.showToast(message: "Shifts created")
            }
        }
    }
    func createDocument(current date: String){
        if let userID = Auth.auth().currentUser?.uid {
            let docRef = db.collection("DoctorCity").document(userID)
            docRef.getDocument(completion: { (snapshot, err) in
                let city = snapshot?.get("city")
                self.db.collection("Doctors").document(city as! String).collection("DoctorsList").document(userID).getDocument(completion: { (docSnapshot, err) in
                    let numberOfShift = docSnapshot?.get("numberOfShift") as! Int
                    for number in 0..<(numberOfShift){
                        self.refArray.append(self.db.collection("Bookings").document(userID)
                            .collection("shift\(number+1)").document(date))
                    }
                    print("Start uploading")
                    self.completeUploading(numberOfShifts: numberOfShift)

                })
            })

        }
    }


    func isShiftsCreated(completionHandler: @escaping (Bool) -> Void){
        if let userID = Auth.auth().currentUser?.uid {
            let docRef = db.collection("Bookings").document(userID).collection("shift1").document(dateString)
            docRef.addSnapshotListener({ (document, err) in
                if let document = document, document.exists {
                    completionHandler(true)
                } else {
                    print("Document does not exist")
                    completionHandler(false)
                }
            })
        }
    }
}

