//
//  AddPatientTabBarVC.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 29/08/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import SVProgressHUD
class AddPatientTabBarVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    let db = Firestore.firestore()
    var refreshControl = UIRefreshControl()
    var docRef: DocumentReference!
    var registration:ListenerRegistration!
    var totalNumberOfButtons = 0
    var selectedShift = 0
    var shifts:[String] = ["Shift One", "Shift Two", "Shift Three", "Shift Four", "Shift Five"]
    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "cleaniqText.png")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        SVProgressHUD.show(withStatus: "Please Wait...")
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        //backgroundView = UIImageView(image: UIImage(named: "plz"))
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.updateTable), userInfo: nil, repeats: false)
    }
    @objc func refresh() {
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    @objc func updateTable(){
        tableView.reloadData()
        SVProgressHUD.dismiss()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let userID = Auth.auth().currentUser?.uid {
            docRef = db.collection("DoctorCity").document(userID)
            docRef.getDocument(completion: { (snapshot, err) in
                let city = snapshot?.get("city")
                self.db.collection("Doctors").document(city as! String).collection("DoctorsList").document(userID).getDocument(completion: { (docSnapshot, err) in
                    let numberOfShift = docSnapshot?.get("numberOfShift") as! Int
                    self.totalNumberOfButtons =  self.shifts.count - numberOfShift

                })
            })
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if registration != nil {
            registration.remove()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        // Get Cell Label
        let indexPath = tableView.indexPathForSelectedRow!
        //let currentCell = tableView.cellForRow(at: indexPath)! as UITableViewCell

        selectedShift = indexPath.row
        performSegue(withIdentifier: "toAddPatient", sender: self)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalNumberOfButtons - 1
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = shifts[indexPath.row]
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let addPatient:AddPatientViewController = segue.destination as! AddPatientViewController
        addPatient.SessionNumber = selectedShift + 1
    }


}
