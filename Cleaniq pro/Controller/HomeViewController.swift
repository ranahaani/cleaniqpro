//
//  HomeViewController.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 30/07/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//
import Firebase
import FirebaseAuth
import UIKit
extension UIView {
    func applyGradient(colours: [UIColor]) -> Void {
        self.applyGradient(colours: colours, locations: nil)
    }
    
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
}

extension UIColor{
    
    static func color1() -> UIColor{
        return Utils.UIColorFromRGB(rgbValue: 0xe9e9e9)
    }
    
    static func color2() -> UIColor{
        
        return Utils.UIColorFromRGB(rgbValue: 0xffffff)
    }
}

class HomeViewController: UIViewController {
    @IBOutlet var botMessage:UILabel!
    @IBOutlet var btn:UIButton!
    var db = Firestore.firestore()
    var mAuth = Auth.auth()
    var nextSessionValue = Int32()
    var numberOfShift = Int32()
    var liveNumberVal = Int32()
    var yy:Int = 0
    var dd:Int = 0
    var mm:Int = 0
    var current:String = String()
    var doctorCity:String = String()
    var bookingStarted:Bool = Bool()
    var allBookings:Bool = Bool()
    var noOfSessionsCompleted = Int32()
    var sessionCompleted = Bool()
     var registration: ListenerRegistration!
   // fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()
    let currentDate: String = {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let Finaldate = dateFormatter.string(from: date)
        return Finaldate
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "cleaniqText.png")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        botMessage.isHidden = true
        btn.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //botMessage.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        if let userID = Auth.auth().currentUser?.uid {
            db.collection("DoctorCity").document(userID).getDocument(completion: { (snapShotOne, errOne) in
                if (snapShotOne?.exists)!{
                    self.doctorCity = snapShotOne!.get("city") as! String
                    self.db.collection("Doctors").document(self.doctorCity).collection("DoctorsList").document(userID).getDocument(completion: { (snapShotTwo, errTwo) in
                        if (snapShotTwo?.exists)!{
                            self.numberOfShift = snapShotTwo!.get("numberOfShift") as! Int32
                            if self.numberOfShift != 0
                            {
                                self.callForUpdate(numberOfShift: self.numberOfShift)
                            }
                        }

                    })
                }
            })
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if registration != nil {
            registration.remove()
        }

    }
    
    
    func callForUpdate(numberOfShift: Int32){
        if let userID = Auth.auth().currentUser?.uid{
            registration = db.collection("Bookings").document(userID).addSnapshotListener({ (snapShotOne, errOne) in
                self.nextSessionValue = snapShotOne!.get("nextSession") as! Int32 // k kon c shift chal rhi h
                self.allBookings = (snapShotOne?.get("allBookings") as? Bool)!
                self.noOfSessionsCompleted = (snapShotOne?.get("noOfSessionsCompleted") as? Int32)!
                if self.allBookings == true {
                    if self.nextSessionValue != 0{
                        self.registration = self.db.collection("Bookings").document(userID)
                            .collection("shift\(self.nextSessionValue)").document(self.currentDate).addSnapshotListener({ (snapShotTwo, errTwo) in
                                if (snapShotTwo?.exists)!{
                                    self.sessionCompleted = (snapShotTwo?.get("completed") as? Bool)!
                                    print(self.sessionCompleted, "sessionCompleted")
                                    self.bookingStarted = (snapShotTwo!.get("bookingStarted") as? Bool)!
                                    print(self.bookingStarted, "bookingStarted")
                                    self.liveNumberVal = snapShotTwo!.get("liveNumber") as! Int32
                                    if ((self.bookingStarted==false) && (self.nextSessionValue==1) && (self.nextSessionValue<=numberOfShift)){

                                        if (numberOfShift==self.nextSessionValue)
                                        {
                                            if self.noOfSessionsCompleted == numberOfShift{
                                                self.btn.isHidden = false
                                                self.botMessage.isHidden = false
                                                self.botMessage.text =  "do you want to end all operation for today?"
                                                self.btn.setTitle("Yes", for: .normal)


                                            }
                                        }
                                        else if (self.sessionCompleted==true){
                                            self.incrementSession()
                                        }

                                    }
                                    else if ((self.bookingStarted == true) && (self.nextSessionValue == 1) && (self.nextSessionValue<=numberOfShift))
                                    {
                                        if (self.liveNumberVal != 0)
                                        {
                                            self.sendValueToAdminFragment();
                                        }
                                        else if (self.liveNumberVal==0)
                                        {
                                            self.btn.isHidden = false
                                            self.botMessage.isHidden = false
                                            self.botMessage.text = "Do you want to start queue for session number \(self.nextSessionValue) ??"
                                            self.btn.setTitle("Yes", for: .normal)
                                        }

                                    }
                                    else if ((self.bookingStarted==false) && (self.nextSessionValue==2) && self.nextSessionValue<=numberOfShift)
                                    {
                                        if (numberOfShift==self.nextSessionValue)
                                        {
                                            if (numberOfShift==self.noOfSessionsCompleted)
                                            {
                                                self.btn.isHidden = false
                                                self.botMessage.isHidden = false
                                                self.botMessage.text =  "do you want to end all operation for today?"
                                                self.btn.setTitle("Yes", for: .normal)
                                            }
                                        }
                                        else if (self.sessionCompleted==true)
                                        {
                                            self.incrementSession();
                                        }
                                    }
                                    else if ((self.bookingStarted == true) && (self.nextSessionValue == 2) && self.nextSessionValue <= numberOfShift)
                                    {
                                        if (self.liveNumberVal != 0)
                                        {

                                           /* self.btn.isHidden = false
                                            self.botMessage.isHidden = false
                                            self.botMessage.text = "Do you want to start queue for session number \(self.nextSessionValue) ??"
                                            self.btn.setTitle("Yes", for: .normal) */
                                            self.sendValueToAdminFragment();
                                        }
                                        else if (self.liveNumberVal==0)
                                        {
                                            self.btn.isHidden = false
                                            self.botMessage.isHidden = false
                                            self.botMessage.text = "Do you want to start queue for session number \(self.nextSessionValue) ."
                                            self.btn.setTitle("yes", for: .normal)
                                        }

                                    }
                                    else if ((self.bookingStarted==false) && (self.nextSessionValue==3) && self.nextSessionValue<=numberOfShift)
                                    {
                                        if (numberOfShift==self.nextSessionValue)
                                        {
                                            self.btn.isHidden = false
                                            self.botMessage.isHidden = false
                                            self.botMessage.text =  "do you want to end all operation for today?"
                                            self.btn.setTitle("Yes", for: .normal)

                                        }
                                        else if (self.sessionCompleted==true)
                                        {
                                            self.incrementSession();
                                        }
                                    }
                                    else if ((self.bookingStarted == true) && self.self.nextSessionValue == 3 && self.nextSessionValue <= numberOfShift)
                                    {
                                        if (self.liveNumberVal != 0)
                                        {
                                            self.sendValueToAdminFragment()
                                        }
                                        else if (self.liveNumberVal==0)
                                        {
                                            self.btn.isHidden = false
                                            self.botMessage.isHidden = false
                                            self.botMessage.text =  "Do you want to start queue for session number "
                                            self.btn.setTitle("Yes", for: .normal)
                                        }
                                    }
                                    else if ((self.bookingStarted==false)&&(self.nextSessionValue==4) && self.nextSessionValue<=numberOfShift)
                                    {
                                        if (numberOfShift==self.nextSessionValue)
                                        {
                                            self.btn.isHidden = false
                                            self.botMessage.isHidden = false
                                            self.botMessage.text =  "do you want to end all operation for today?"
                                            self.btn.setTitle("Yes", for: .normal)
                                        }
                                        else if (self.sessionCompleted==true)
                                        {
                                            self.incrementSession();
                                        }
                                    }
                                    else if ((self.bookingStarted == true) && self.self.nextSessionValue == 4 && self.nextSessionValue<=numberOfShift)
                                    {
                                        if (self.liveNumberVal != 0)
                                        {
                                            self.sendValueToAdminFragment()
                                        }
                                        else if (self.liveNumberVal==0)
                                        {
                                            self.btn.isHidden = false
                                            self.botMessage.isHidden = false
                                            self.botMessage.text =  "Do you want to start queue for session number \(self.nextSessionValue)  ??"
                                            self.btn.setTitle("Yes", for: .normal)
                                        }
                                    }
                                    else if ((self.bookingStarted==false)&&(self.nextSessionValue==5)&&self.nextSessionValue<=numberOfShift)
                                    {
                                        if (numberOfShift==self.nextSessionValue)
                                        {
                                            self.btn.isHidden = false
                                            self.botMessage.isHidden = false
                                            self.botMessage.text =  "do you want to end all operation for today?"
                                            self.btn.setTitle("Yes", for: .normal)
                                        }
                                        else if (self.sessionCompleted==true)
                                        {
                                            self.incrementSession()
                                        }

                                    }
                                    else if ((self.bookingStarted == true) && self.nextSessionValue == 5 && (self.nextSessionValue<=numberOfShift))
                                    {
                                        if (self.liveNumberVal != 0)
                                        {
                                            self.sendValueToAdminFragment()
                                        }
                                        else if (self.liveNumberVal==0)
                                        {
                                            self.btn.isHidden = false
                                            self.botMessage.isHidden = false
                                            self.botMessage.text =  "Do you want to start queue for session number \(self.nextSessionValue)  ??"
                                            self.btn.setTitle("Yes", for: .normal)
                                        }
                                    }
                                    else
                                    {
                                        //There's nothing
                                    }
                            }
                        })
                    }
                }
                else{
                    print("All Bookings are closed")
                    self.btn.isHidden = false
                    self.botMessage.isHidden = false
                    self.botMessage.text = "All Bookings are closed"
                    self.btn.setTitle("Start Booking", for: .normal)
                }
            })
        }
    }
    private func sendValueToAdminFragment() {
         performSegue(withIdentifier: "yes", sender: (Any).self)
    }
     func incrementSession() {
        if let userID = Auth.auth().currentUser?.uid {
            db.collection("Bookings").document(userID).updateData(["nextSession" : nextSessionValue+1])
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (registration != nil)
        {
            registration.remove();
        }
        //let button = sender as? UIButton
        if bookingStarted == true{
            let AdminPanel = segue.destination as! AdminPanelViewController
            AdminPanel.nextSessionValue = nextSessionValue
            AdminPanel.numberOfShift = numberOfShift
        }
    }
    @IBAction func startBookingButtonPressed(_ sender: UIButton) {
        do{

        if ((allBookings==false)&&(nextSessionValue==1))
        {
            performSegue(withIdentifier: "toSession", sender: nil)
        }
        else if ((bookingStarted == true) && nextSessionValue == 1){
            performSegue(withIdentifier: "yes", sender: (Any).self)
        }
        else if ((bookingStarted==false)&&(nextSessionValue==2))
        {
            performSegue(withIdentifier: "toSession", sender: nil)
        }
        else if ((bookingStarted == true) && nextSessionValue == 2)
        {
            performSegue(withIdentifier: "yes", sender: (Any).self)
        }
        else if ((bookingStarted==false)&&(nextSessionValue==3))
        {    performSegue(withIdentifier: "toSession", sender: nil)
        }
        else if ((bookingStarted == true) && nextSessionValue == 3)
        {
            performSegue(withIdentifier: "yes", sender: (Any).self)
        }
        else if ((bookingStarted==false)&&(nextSessionValue==4))
        {
            performSegue(withIdentifier: "toSession", sender: nil)
        }
        else if ((bookingStarted == true) && nextSessionValue == 4)
        {
            performSegue(withIdentifier: "yes", sender: (Any).self)
        }
        else if ((bookingStarted==false)&&(nextSessionValue==5))
        {
            performSegue(withIdentifier: "toSession", sender: nil)
        }
        else if ((bookingStarted == true) && nextSessionValue == 5)
        {
            performSegue(withIdentifier: "yes", sender: (Any).self)
        }
        }
//        catch (Error)
//        {
//            print(Error.self, "Clicked error")
//        }
    }
    
}
