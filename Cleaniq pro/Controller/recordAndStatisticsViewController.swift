//
//  recordAndStatisticsViewController.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 30/09/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit

class recordAndStatisticsViewController: UIViewController , UIPickerViewDelegate, UIPickerViewDataSource,UITextFieldDelegate{

    var datePicker = UIDatePicker()
    var shiftPicker = UIPickerView()
    @IBOutlet weak var dateLabel: UITextField!
    var shifts:[String] = ["Shift One", "Shift Two", "Shift Three", "Shift Four", "Shift Five"]

    @IBOutlet weak var shiftLabel: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.datePickerMode = .date
        shiftPicker.delegate = self
        shiftPicker.dataSource = self
        let shiftToolBar = UIToolbar()
        shiftToolBar.sizeToFit()
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
       // shiftToolBar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        shiftLabel.inputAccessoryView = shiftToolBar
        shiftLabel.inputView = shiftPicker
        dateLabel.inputAccessoryView = toolbar
        dateLabel.inputView = datePicker
        // Do any additional setup after loading the view.
    }

    @IBAction func donePressed(_ sender: Any) {
    }
    func showDatePicker(){


    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return shifts.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return shifts[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        shiftLabel.text = shifts[row]
        self.view.endEditing(true)
    }
    @objc func donedatePicker(){

        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        dateLabel.text = " \(formatter.string(from: datePicker.date))"
        self.view.endEditing(true)
    }

    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }


}
