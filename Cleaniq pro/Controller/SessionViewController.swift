
//
//  SessionViewController.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 19/09/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit
import SVProgressHUD
import FirebaseAuth
import FirebaseFirestore
class SessionViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let db = Firestore.firestore()
    var docRef: DocumentReference!
    var registration:ListenerRegistration!
    var totalNumberOfButtons = 0
    let currentDate: String = {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let Finaldate = dateFormatter.string(from: date)
        return Finaldate
    }()
    let refreshControl = UIRefreshControl()
    var shifts:[String] = ["Shift One", "Shift Two", "Shift Three", "Shift Four", "Shift Five"]
    @IBOutlet weak var tableView:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show(withStatus: "Please Wait...")
        //backgroundView = UIImageView(image: UIImage(named: "plz"))
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.updateTable), userInfo: nil, repeats: false)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
    }
    @objc func refresh() {
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    @objc func updateTable(){
        tableView.reloadData()
        SVProgressHUD.dismiss()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let userID = Auth.auth().currentUser?.uid {
             docRef = db.collection("DoctorCity").document(userID)
              docRef.getDocument(completion: { (snapshot, err) in
                let city = snapshot?.get("city")
                self.db.collection("Doctors").document(city as! String).collection("DoctorsList").document(userID).getDocument(completion: { (docSnapshot, err) in
                    let numberOfShift = docSnapshot?.get("numberOfShift") as! Int
                    self.totalNumberOfButtons =  self.shifts.count - numberOfShift

                })
            })
        }
    }
    @IBAction func startBookingPressed(_ sender: UIButton) {
        db.collection("Bookings").document((Auth.auth().currentUser?.uid)!).updateData(["allBookings" : true]) { (err) in
            if err == nil{
                var liveUpdate = Dictionary<String,Any>()
                liveUpdate["timestamp"] = FieldValue.serverTimestamp();
                liveUpdate["message"] = "Doctor opens Bookings for Today."
                self.db.collection("Bookings").document((Auth.auth().currentUser?.uid)!).collection("shift1").document(self.currentDate).collection("liveUpdates").addDocument(data: liveUpdate, completion: { (err2) in
                    if err2 == nil{
                        SVProgressHUD.showSuccess(withStatus: "All Bookings Started")
                    }
                })
            }
        }

        performSegue(withIdentifier: "backToHome", sender: nil)
        

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if registration != nil {
            registration.remove()
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalNumberOfButtons - 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) 
        cell.textLabel?.text = shifts[indexPath.row]
        let switchView = UISwitch(frame: .zero)
        switchView.setOn(false, animated: true)
        switchView.tag = indexPath.row // for detect which row switch Changed
        switchView.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
        cell.accessoryView = switchView
        return cell
    }
    @objc func switchChanged(_ sender : UISwitch!){
        db.collection("Bookings").document((Auth.auth().currentUser?.uid)!)
            .collection("shift\(sender.tag + 1)").document(currentDate).updateData(["bookingStarted":sender.isOn ? true : false])
        print("table row switch Changed \(sender.tag)")
        print("The switch is \(sender.isOn ? "ON" : "OFF")")
    }

    func updateData(indexPath: IndexPath){
        //var updateData = Dictionary<String,Bool>()



    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
