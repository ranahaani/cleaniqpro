//
//  AdminPanel1ViewController.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 25/08/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit
import FirebaseFirestore
import FirebaseAuth
import SVProgressHUD
class AdminPanelViewController: UIViewController {
    private var ptDocRef:DocumentReference!
    @IBOutlet weak var textLive: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var Processing: UILabel!
    @IBOutlet weak var Age: UILabel!
    @IBOutlet weak var tokenNumber: UILabel!
    @IBOutlet weak var meetAndNext: UIButton!
    @IBOutlet weak var startQueue: UIButton!
    @IBOutlet weak var markPaid: UIButton!
    @IBOutlet weak var AddNote: UIButton!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var markAbsent: UIButton!
    @IBOutlet weak var takeABreak: UIButton!
    @IBOutlet weak var endThisSession: UIButton!
    @IBOutlet weak var patientInfo: UIView!
    @IBOutlet weak var patientNumber: UILabel!
    @IBOutlet weak var imageViewPatient: UIImageView!
    var queueStatus = ""
    var bookingStarted = true
    private var tokenVals:Int = 0
    var liveNumberVal:Int32 = 0
    private var patientName:String = ""
    var device_token:String = ""
    private var patientaverage:Int32 = 0
    var x:Int32 = 0
     var userKey:String = ""
    var docKey:String = ""
    var nextSessionValue:Int32 = 0
    var numberOfShift:Int32 = 0
    let db = Firestore.firestore()
    var statusUpdate = ""
    let currentDate: String = {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let Finaldate = dateFormatter.string(from: date)
        return Finaldate
    }()
    var completed:Bool!
    var registration:ListenerRegistration!
    var documentReference:DocumentReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        let logo = UIImage(named: "cleaniqText.png")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        //tokenNumber.layer.cornerRadius = 50
        //tokenNumber.layer.masksToBounds = true
        patientInfo.isHidden = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let userID = Auth.auth().currentUser?.uid {
            ptDocRef = db.collection("Bookings").document(userID).collection("shift\(nextSessionValue)").document(currentDate)

            db.collection("Bookings").document(userID)
                .collection("shift\(nextSessionValue)").document(currentDate).getDocument(completion: { (documentSnapshot, err) in
                    if (documentSnapshot?.exists)!
                    {
                        //getting field present in current date session
                        //let numberofBooking = documentSnapshot?.get("tokens") as? String
                        let liveNumber = documentSnapshot?.get("liveNumber") as? Int32
                        self.queueStatus = (documentSnapshot?.get("queueStatus") as? String)!
                        self.completed = (documentSnapshot?.get("completed") as? Bool)!
                        self.bookingStarted = self.completed
                        if (self.completed != nil)
                        {
                            if (self.completed == true)
                            {   self.textLive.isHidden = false
                                self.textLive.text = "Session has been completed"
                                self.startQueue.isEnabled = false
                                self.markAbsent.isEnabled = false
                                self.meetAndNext.isEnabled = false
                                self.takeABreak.isEnabled = false
                                self.endThisSession.isEnabled = false

                            }
                            else
                            {
                                if (liveNumber==0)
                                {
                                    self.textLive.text = "Start Queue ?"
                                    self.startQueue.isEnabled = true
                                }
                                else
                                {
                                    //If queue is started then show the live patient info
                                    self.showPatientInfo(liveNumber: liveNumber!)
                                    self.startQueue.isEnabled = false
                                }
                                self.markAbsent.isEnabled = true
                                self.meetAndNext.isEnabled = true
                                self.takeABreak.isEnabled = true
                                self.endThisSession.isEnabled = true
                            }
                        }
                    }
                    else{}
                })
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if completed == true{
            let list:PatientListViewController = segue.destination as! PatientListViewController
            list.shiftNumber = nextSessionValue
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if registration != nil{
            registration.remove()
        }
    }


    @IBAction func addNotePressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "Add Note", message: "Please add a note", preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save",style: .default){
            (action: UIAlertAction!) -> Void in
            let textField1 = alert.textFields![0] as UITextField
            if (self.docKey.count == 10)
            {
                ////update patient value to firedtore
                self.documentReference.updateData(["patientNote" : textField1.text!], completion: { (err) in
                    if err == nil{
                        SVProgressHUD.showSuccess(withStatus: "Successfully Submitted")
                    }
                    else{SVProgressHUD.showError(withStatus: err?.localizedDescription)}
                })
            }
            else
            {
                //update patient value to firedtore to Patients Tab
                var dictionary = Dictionary<String, Any>()

                dictionary["note"] = textField1.text
                dictionary["doctorUID"] = Auth.auth().currentUser?.uid
                self.db.collection("Patients").document(self.docKey).collection("Notes").addDocument(data: dictionary, completion: { (err) in
                    if err == nil {
                        SVProgressHUD.showSuccess(withStatus: "Note Added")
                    }
                    else {SVProgressHUD.showError(withStatus: err?.localizedDescription)}
                })
                print("100")
            }



        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default) { (action: UIAlertAction!) -> Void in}

        alert.addTextField {(textField1: UITextField!) -> Void in
            textField1.placeholder = ""
        }

        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert,animated: true,completion: nil)
    }
    
    @IBAction func markPaidPressed(_ sender: Any) {
        let alert = UIAlertController(title: "Add Note", message: "Please add a note", preferredStyle: .alert)
        alert.addTextField {(textField1: UITextField!) -> Void in
            textField1.placeholder = ""
            textField1.keyboardType = .numberPad
        }

        let saveAction = UIAlertAction(title: "Save",style: .default){
            (action: UIAlertAction!) -> Void in
            let textField1 = alert.textFields![0] as UITextField
            self.documentReference.updateData(["patientFee":Int(textField1.text!)!])
            print(textField1.text!)
            SVProgressHUD.showSuccess(withStatus: "Updated")
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default) { (action: UIAlertAction!) -> Void in}


        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert,animated: true,completion: nil)
    }

    @IBAction func EndThisSessionPressed(_ sender: UIButton) {
        alerController(set: "Do you want to End This Session?", set: "End This Session", for: sender)
    }
    @IBAction func listButtonPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "toList", sender: nil)
    }
    @IBAction func takeBreakPressed(_ sender: UIButton) {
        alertControllerWithTextField(set: "Enter Your Reason ", set: "Status Update", for: sender)
    }

    @IBAction func startQueuePressed(_ sender: Any) {
        if let userID = Auth.auth().currentUser?.uid{
            db.collection("Bookings").document(userID).collection("shift\(nextSessionValue + 1)").document(currentDate).getDocument(completion: { (documentSnapshot, err) in
                if (documentSnapshot?.exists)!
                {


                    let tokenTot = documentSnapshot?.get("tokens") as? Int32;//Getting total number of booking
                    if (tokenTot! != 0)
                    {
                        //Now setting 1 to field liveNumber to firestore
                        self.db.collection("Bookings").document(userID).collection("shift\(self.nextSessionValue + 1)").document(self.currentDate)
                            .updateData(["liveNumber" : 1], completion: { (err) in
                                if (err == nil){
                                    self.showPatientInfo(liveNumber: 1)

                                    var liveUpdate = Dictionary<String,Any>()
                                    liveUpdate["timestamp"] = FieldValue.serverTimestamp();
                                    liveUpdate["message"] = "Doctor opens Bookings for Today."
                                    self.db.collection("Bookings").document((Auth.auth().currentUser?.uid)!).collection("shift1").document(self.currentDate).collection("liveUpdates").addDocument(data: liveUpdate, completion: { (err2) in
                                        if err2 == nil{
                                            SVProgressHUD.showSuccess(withStatus: "Queue Started for session \(self.nextSessionValue + 1)")
                                        }
                                    })

                                }
                                else{
                                    SVProgressHUD.showError(withStatus: err?.localizedDescription)
                                }
                            })
                    }
                    else {
                        SVProgressHUD.showInfo(withStatus: "Oops! No Bookings!")
                    }
                    SVProgressHUD.dismiss()
                }
                else{
                    SVProgressHUD.showError(withStatus: err?.localizedDescription)
                }
            })
        }
    }

    @IBAction func markAbsentPressed(_ sender: Any) {
        //SVProgressHUD.show(withStatus: "Please Wait...")
        if documentReference != nil {
            documentReference.updateData([ "patientStatus":  "Absent"], completion: { (err) in
                if err == nil{
                    self.db.runTransaction({ (transaction, errorPointer) -> Any? in
                        let sfDocument: DocumentSnapshot
                        do {
                            try sfDocument = transaction.getDocument(self.ptDocRef)
                            self.tokenVals = sfDocument.get("tokens") as! Int
                            self.liveNumberVal = sfDocument.get("liveNumber") as! Int32
                            self.liveNumberVal += 1
                            if (self.liveNumberVal != 0 && self.tokenVals != 0) {
                                if (self.liveNumberVal <= self.tokenVals) {
                                    transaction.updateData(["liveNumber": self.liveNumberVal],forDocument: self.ptDocRef)
                                } else {
                                    self.showToast(message: "No Patient Left To Call.")
                                }
                            }

                        } catch let fetchError as NSError {
                            errorPointer?.pointee = fetchError
                            return nil
                        }

                        return nil
                    }) { (object, error) in
                        if let error = error {
                            print("\(error)")
                            self.sendPushNotificationToAbsent(liveNumber: self.liveNumberVal-1)
                            self.decreaseChance(livenumber: self.liveNumberVal - 1)
                            SVProgressHUD.showError(withStatus: error.localizedDescription)
                        } else {


                            var liveUpdate = Dictionary<String,Any>()
                            liveUpdate["timestamp"] = FieldValue.serverTimestamp();
                            liveUpdate["message"] = "Doctor started consulting token no. \(self.liveNumberVal)"
                            self.db.collection("Bookings").document((Auth.auth().currentUser?.uid)!).collection("shift1").document(self.currentDate).collection("liveUpdates").addDocument(data: liveUpdate, completion: { (err2) in
                                if err2 == nil{
                                    SVProgressHUD.showSuccess(withStatus: "Absent marked")
                                    self.sendPushNotificationToAbsent(liveNumber: self.liveNumberVal-1)
                                    self.decreaseChance(livenumber: self.liveNumberVal - 1)
                                    self.showPatientInfo(liveNumber: self.liveNumberVal)
                                }
                            })
                        }
                    }
                }
            })
        }
    }
    @IBAction func meetAndNextPressed(_ sender: Any) {
        if documentReference != nil {
            documentReference.updateData([ "patientStatus":  "Completed"], completion: { (err) in
                if err == nil{
                    self.db.runTransaction({ (transaction, errorPointer) -> Any? in
                        let sfDocument: DocumentSnapshot
                        do {
                            try sfDocument = transaction.getDocument(self.ptDocRef)
                            self.tokenVals = sfDocument.get("tokens") as! Int
                            self.liveNumberVal = sfDocument.get("liveNumber") as! Int32
                            self.liveNumberVal += 1

                            if (self.liveNumberVal != 0 && self.tokenVals != 0) {
                                if (self.liveNumberVal <= self.tokenVals) {
                                    transaction.updateData(["liveNumber": self.liveNumberVal],forDocument: self.ptDocRef)
                                    SVProgressHUD.dismiss()
                                } else {
                                    SVProgressHUD.showInfo(withStatus: "No Patient Left To Call.")
                                    SVProgressHUD.dismiss()
                                }
                            }
                            SVProgressHUD.dismiss()

                        } catch let fetchError as NSError {
                            errorPointer?.pointee = fetchError
                            return nil
                        }

                        return nil
                    }) { (object, error) in
                        if error == nil {
                            var liveUpdate = Dictionary<String,Any>()
                            liveUpdate["timestamp"] = FieldValue.serverTimestamp();
                            liveUpdate["message"] = self.statusUpdate
                            self.db.collection("Bookings").document((Auth.auth().currentUser?.uid)!).collection("shift1").document(self.currentDate).collection("liveUpdates").addDocument(data: liveUpdate, completion: { (err2) in
                                if err2 == nil{
                                    SVProgressHUD.showSuccess(withStatus: "Next...")
                            SVProgressHUD.dismiss()
                            self.sendDataToMsg(livenumber: self.liveNumberVal)
                            self.showPatientInfo(liveNumber: self.liveNumberVal)

                                }
                            })


                        } else {
                            SVProgressHUD.showError(withStatus: err?.localizedDescription)
                            SVProgressHUD.dismiss()
                        }
                    }
                }
            })
        }
    }
    func sendPushNotificationToAbsent(liveNumber: Int32){}
    func decreaseChance(livenumber: Int32){}
    func alerController(set alertMessage: String, set title:String, for sender: UIButton){
        let alert = UIAlertController(title: title, message: alertMessage, preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Yes",style: .default) { (action: UIAlertAction!)->
            Void in
            if sender.tag == 0 {
                self.EndSessionPressAction()
            }
        }
        let cancelAction = UIAlertAction(title: "No",style: .default) { (action: UIAlertAction!) -> Void in
            print("cancel")
        }
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert,animated: true,completion: nil)
    }

    func alertControllerWithTextField(set AlertMessage: String,set title:String, for sender: UIButton){
        let alert = UIAlertController(title: title, message: AlertMessage, preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save",style: .default){
            (action: UIAlertAction!) -> Void in
            let textField1 = alert.textFields![0] as UITextField
            //print(sender.currentTitle)
            if sender.currentTitle == "TAKE A BREAK"{
                if textField1.text != nil {
                    self.statusUpdate = textField1.text!
                    self.takeABreakAction()
                }
                else{
                    self.showToast(message: "Please Put some Value")
                }
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default) { (action: UIAlertAction!) -> Void in}

        alert.addTextField {(textField1: UITextField!) -> Void in
            textField1.placeholder = ""
        }

        alert.addAction(saveAction)
        alert.addAction(cancelAction)

        present(alert,animated: true,completion: nil)

    }
    func takeABreakAction (){
        if let userID = Auth.auth().currentUser?.uid{
            db.collection("Bookings").document(userID).collection("shift\(nextSessionValue)").document(currentDate).updateData(["queueStatus":statusUpdate], completion: { (err) in
                if (err != nil) {
                    self.showToast(message: "Unable to Take a break")
                }
                else{
                    var liveUpdate = Dictionary<String,Any>()
                    liveUpdate["timestamp"] = FieldValue.serverTimestamp();
                    liveUpdate["message"] = self.statusUpdate
                    self.db.collection("Bookings").document((Auth.auth().currentUser?.uid)!).collection("shift1").document(self.currentDate).collection("liveUpdates").addDocument(data: liveUpdate, completion: { (err2) in
                        if err2 == nil{
                            SVProgressHUD.showSuccess(withStatus: "Status Saved")
                        }
                    })
                }
            })
        }
    }


    func EndSessionPressAction(){
        let batch:WriteBatch = Firestore.firestore().batch()
        var updateData = Dictionary<String, Any>()
        updateData["queueStatus"] = "completed"
        updateData["bookingStarted"] = false
        updateData["completed"] = true

        if let userID = Auth.auth().currentUser?.uid{
            batch.updateData(updateData, forDocument: db.collection("Bookings").document(userID).collection("shift\(nextSessionValue)").document(currentDate))
            batch.updateData(["noOfSessionsCompleted":nextSessionValue], forDocument: db.collection("Bookings").document(userID))
            batch.commit() { err in
                if let err = err {
                    print("Error writing batch \(err)")
                } else {
                    SVProgressHUD.showSuccess(withStatus:  "Session Closed!")
                    var liveUpdate = Dictionary<String,Any>()
                    liveUpdate["timestamp"] = FieldValue.serverTimestamp();
                    liveUpdate["message"] = "Doctor started consulting token no.1."
                    batch.setData(liveUpdate, forDocument: self.db.collection("Bookings").document(userID).collection("shift1").document(self.currentDate).collection("liveUpdates").document())
                    self.performSegue(withIdentifier: "AdminPanelToHome", sender: nil)
                }
            }
        }

    }
    func showPatientInfo(liveNumber: Int32){
        patientInfo.isHidden = false
        SVProgressHUD.show(withStatus: "Please Wait...")
        if let userID = Auth.auth().currentUser?.uid{
            db.collection("Bookings").document(userID).collection("shift\(nextSessionValue)").document(currentDate).collection("appointments").whereField("tokens", isEqualTo: liveNumber).limit(to: 1).getDocuments(completion: { (DocumentSnapshot, err) in
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in DocumentSnapshot!.documents {
                        self.patientName = (document.get("patientName") as? String!)!
                        let patientNumber = document.get("patientPNumber") as? String
                        let patientGender = document.get("patientGender") as? String
                        let patientStatus = document.get("patientStatus") as? String
                        let pToken = document.get("tokens")as? String
                        let patientAge = document.get("patientAge")as? String
                        self.docKey = document.documentID
                        self.documentReference = document.reference
                        self.customerName.text = self.patientName
                        self.patientNumber.text = patientNumber
                        self.tokenNumber.text = pToken
                        self.Processing.text = patientStatus
                        //txtPStatus.setText(patientStatus)
                        self.Age.text = "Age -\(patientAge ?? "Age")"

                        if ((patientGender?.caseInsensitiveCompare("Male")) == ComparisonResult.orderedSame){
                            self.imageViewPatient.image = #imageLiteral(resourceName: "boy")
                        }else if ((patientGender?.caseInsensitiveCompare("Female")) == ComparisonResult.orderedSame){
                            self.imageViewPatient.image = #imageLiteral(resourceName: "girl")
                        }
                        else if patientGender?.caseInsensitiveCompare("Other") == ComparisonResult.orderedSame{
                            self.imageViewPatient.image = #imageLiteral(resourceName: "girl")
                        }


                        if (patientStatus?.caseInsensitiveCompare("Booked")) == (ComparisonResult.orderedSame){
                            SVProgressHUD.show(withStatus: "Please wait....")
                            self.documentReference.updateData(["patientStatus" : "Processing"], completion: { (err) in
                                if (err != nil){print(err?.localizedDescription ?? "Error in Patient Status")}
                                else{
                                    self.showPatientInfo(liveNumber: liveNumber)
                                    SVProgressHUD.dismiss()
                                }
                            })
                        }
                        else if (patientStatus?.caseInsensitiveCompare("Cancelled")) == (ComparisonResult.orderedSame){
                            self.callNextPatient();
                            SVProgressHUD.dismiss()

                        }
                        if (patientStatus?.caseInsensitiveCompare("Processing")) == (ComparisonResult.orderedSame){
                            self.Processing.fadeTransition(0.4)
                            SVProgressHUD.dismiss()
                        }
                    }
                }

            })
        }
    }
    
    func sendDataToMsg(livenumber: Int32){}
    func callNextPatient(){
        self.db.runTransaction({ (transaction, errorPointer) -> Any? in
            let sfDocument: DocumentSnapshot
            do {
                try sfDocument = transaction.getDocument(self.ptDocRef)
                self.tokenVals = sfDocument.get("tokens") as! Int
                self.liveNumberVal = sfDocument.get("liveNumber") as! Int32
                self.liveNumberVal += 1
                if (self.liveNumberVal != 0 && self.tokenVals != 0) {
                    if (self.liveNumberVal <= self.tokenVals) {
                        transaction.updateData(["liveNumber": self.liveNumberVal],forDocument: self.ptDocRef)
                    } else {
                        SVProgressHUD.showInfo(withStatus: "No Patient to call")
                    }
                }

            } catch let fetchError as NSError {
                errorPointer?.pointee = fetchError
                return nil
            }

            return nil
        }) { (object, error) in
            if error == nil {

                var liveUpdate = Dictionary<String,Any>()
                liveUpdate["timestamp"] = FieldValue.serverTimestamp();
                liveUpdate["message"] = "Doctor started consulting token no. \(self.liveNumberVal)"
                self.db.collection("Bookings").document((Auth.auth().currentUser?.uid)!).collection("shift1").document(self.currentDate).collection("liveUpdates").addDocument(data: liveUpdate, completion: { (err2) in
                    if err2 == nil{
                        SVProgressHUD.showSuccess(withStatus: "Next Patient Called")
                        self.sendDataToMsg(livenumber: self.liveNumberVal)
                        self.showPatientInfo(liveNumber: self.liveNumberVal)
                    }
                })



            } else {
                SVProgressHUD.dismiss()
                self.sendPushNotificationToAbsent(liveNumber: self.liveNumberVal-1)
                self.decreaseChance(livenumber: self.liveNumberVal - 1)
            }
        }
    }
}


extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            kCAMediaTimingFunctionEaseInEaseOut)
        animation.type = kCATransitionFade
        animation.duration = duration
        layer.add(animation, forKey: kCATransitionFade)
    }
}

extension UIViewController {

    func showToast(message : String) {

        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 95, y: self.view.frame.size.height-150, width: 220, height: 50))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Montserrat-Light", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)

        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })

        toastLabel.translatesAutoresizingMaskIntoConstraints = false
        toastLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        //toastLabel.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 20).isActive = true
        toastLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        toastLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -60).isActive = true
    }

}

