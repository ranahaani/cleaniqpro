//
//  FirstViewController.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 28/07/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import SVProgressHUD
import FirebaseFirestore
class myProfileViewController: UIViewController,UITableViewDelegate, UITableViewDataSource {
    let cellPlaceHolderlabel = ["Name", "Clinic","Status", "Tiltle", "Address", "Contact No", "Shifts"]
    let db = Firestore.firestore()
    var CellNameLabel = ["Name", "Clinic","Status", "Tiltle", "Address", "Contact No", ""]{
        didSet{
            
        }
    }
    var shiftNumber = "0"
    var reasonForShiftChange = ""
    var shiftNumberToChange = "0"
    var TimeStamp = Timestamp()
    
    @IBOutlet weak var myProfiletabelView: UITableView!
    //var data = readAndWriteDataOnDatabase()
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.updateTable), userInfo: nil, repeats: false)
        retriveData()
         self.myProfiletabelView.dataSource = self
        
        
    }
    func retriveData(){
        if let userID = Auth.auth().currentUser?.uid {
            db.collection("DoctorCity").document(userID)
                .getDocument { (snapshot, error ) in
                    
                    if let document = snapshot {
                        let city = document.get("city")
                        self.db.collection("Doctors").document(city as! String).collection("DoctorsList").document(userID).getDocument(completion: { (snapshota, error) in
                            if let fDocoment = snapshota {
                                //final document
                                print(fDocoment.get("doctorName") ?? "Name")
                                self.CellNameLabel[0].removeAll()
                                self.CellNameLabel[0].append(fDocoment.get("doctorName") as! String)
                                self.CellNameLabel[1].removeAll()
                                self.CellNameLabel[1].append(fDocoment.get("drClinicName") as! String)
                                self.CellNameLabel[2].removeAll()
                                self.CellNameLabel[2].append(fDocoment.get("doctorStatus") as! String)
                                self.CellNameLabel[3].removeAll()
                                self.CellNameLabel[3].append(fDocoment.get("doctorTitle") as! String)
                                self.CellNameLabel[4].removeAll()
                                self.CellNameLabel[4].append(fDocoment.get("drAddress") as! String)
                                self.CellNameLabel[5].removeAll()
                                let contactNo = fDocoment.get("contactNo")!

                                self.CellNameLabel[5].append(String(describing: contactNo))
                                self.TimeStamp = fDocoment.get("planValidTill") as! Timestamp
                                self.CellNameLabel[6].removeAll()
                                self.CellNameLabel[6].append(String(fDocoment.get("numberOfShift") as! Int))
                                
                                self.myProfiletabelView.reloadData()
                            }
                            else{
                                print("There's no data against current doctor")
                            }
                        })
                        
                        
                    } else {
                        
                        print("Document does not exist")
                    }
            }
        }
    }
}
public extension Int {
    
    public var seconds: DispatchTimeInterval {
        return DispatchTimeInterval.seconds(self)
    }
    
    public var second: DispatchTimeInterval {
        return seconds
    }
    
    public var milliseconds: DispatchTimeInterval {
        return DispatchTimeInterval.milliseconds(self)
    }
    
    public var millisecond: DispatchTimeInterval {
        return milliseconds
    }
    
}

public extension DispatchTimeInterval {
    public var fromNow: DispatchTime {
        return DispatchTime.now() + self
    }
}


// MARK: - UITableViewDataSource

extension myProfileViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellPlaceHolderlabel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldInTableViewCell") as! myProfieTabelViewCellTableViewCell
        if indexPath[1] % 2  == 0{
             cell.backgroundColor = #colorLiteral(red: 0.9088078997, green: 0.9088078997, blue: 0.9088078997, alpha: 1)
            print(indexPath[1])
        }
        else{
             cell.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        cell.selectionStyle = .none
        cell.placeHolderLabel.text = cellPlaceHolderlabel[indexPath[1]]
        cell.TextLabel.text = self.CellNameLabel[indexPath[1]]
        
        cell.delegate = self
        return cell
    }
    
    
}
// MARK: - TextFieldInTableViewCellDelegate

extension myProfileViewController: TextFieldInTableViewCellDelegate {
    func textFieldInTableViewCell(didSelect cell:myProfieTabelViewCellTableViewCell) {
        if let indexPath = myProfiletabelView.indexPath(for: cell){
            let alertController = UIAlertController(title: "Edit", message: "\(cellPlaceHolderlabel[indexPath[1]]) to be edited", preferredStyle: .alert)
            
            //the confirm action taking the inputs
            let confirmAction = UIAlertAction(title: "Confirm", style: .default) { (_) in
                
                //getting the input values from user
                self.CellNameLabel[indexPath[1]] = (alertController.textFields?[0].text)!
                self.myProfiletabelView.reloadRows(at: [indexPath], with: .automatic)
                
                // you have to check here whether u need to use 1st index of input or not
                if self.cellPlaceHolderlabel[indexPath[1]] == "Shifts"{

                self.shiftNumber = (alertController.textFields?[1].text)!
                self.reasonForShiftChange = (alertController.textFields?[2].text)!
                    
                }
            }
            
            if cellPlaceHolderlabel[indexPath[1]] == "Shifts"{
                alertController.addTextField { (textField) in
                    textField.placeholder = "Which Shift you want to Update"
                }
                alertController.addTextField { (textField) in
                    textField.placeholder = "Reason (Update, Delete, Create)"
                }
                
            }
            alertController.addTextField { (textField) in
                textField.placeholder = "Enter \(self.cellPlaceHolderlabel[indexPath[1]])"
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
            //adding the action to dialogbox
            alertController.addAction(confirmAction)
            alertController.addAction(cancelAction)
            //finally presenting the dialog box
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    @objc func updateTable(){
        SVProgressHUD.dismiss()
    }
    func textFieldInTableViewCell(cell:myProfieTabelViewCellTableViewCell, editingChangedInTextField newText:String) {
        if let indexPath = myProfiletabelView.indexPath(for: cell){
            print("editingChangedInTextField: \"\(newText)\" in cell: \(indexPath)")
        }
    }
   
    
}

