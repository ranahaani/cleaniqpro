//
//  SettingSViewControllerTableViewController.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 02/08/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit
import FirebaseAuth
import SVProgressHUD
import FirebaseFirestore
//import Razorpay
class SettingsTableViewController: UITableViewController{
    //var razorpay: Razorpay!
    var mAuth = Auth.auth()
    var documentReference:DocumentReference!
    var nextSessionValue = Int32()
    var numberOfShift = Int32()
    var liveNumberVal = Int32()
    var yy:Int = 0
    var dd:Int = 0
    var mm:Int = 0
    let datePicker = UIDatePicker()
    var doctorCity:String = String()
    var bookingStarted:Bool = Bool()
    var allBookings:Bool = Bool()
    var noOfSessionsCompleted = Int32()
    var sessionCompleted = Bool()
    var registration: ListenerRegistration!
    // fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()
    let currentDate: String = {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let Finaldate = dateFormatter.string(from: date)
        return Finaldate
    }()
    var city:String!
    let db = Firestore.firestore()
    let sections: [String] = ["Booking Turn On/Off", "Patient Limit", "General Settings", "Payments"]
    var s1Data: [String] = []
    var s2Data: [String] = []
    let s3Data: [String] = ["FAQ's", "Records & Statistics", "Call Off", "Reffer a Doctor", "Change Password","Contact Us"]
    override func viewDidLoad() {
        let logo = UIImage(named: "cleaniqText.png")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        super.viewDidLoad()
        getTotalNumberOfShift()
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.updateTable), userInfo: nil, repeats: false)
        if let userID = mAuth.currentUser?.uid {
            db.collection("DoctorCity").document(userID).getDocument(completion: { (documentSnapshot, err) in
                if (documentSnapshot?.exists)!
                {
                    self.city = documentSnapshot?.get("city") as! String

                    //cleaniqPayments(city);

                }
            })
        }
        //razorpay = Razorpay.initWithKey("rzp_test_J0DePpEUPBz9YI", andDelegate: self)
    }

//    internal func showPaymentForm(){
//        let options: [String:Any] = [
//            "amount" : "100",
//            "description": "purchase description",
//            "image": "https://url-to-image.png",
//            "name": "business or product name",
//            "prefill": [
//            "contact": "9797979797",
//            "email": "foo@bar.com"
//            ],
//            "theme": [
//                "color": "#F37254"
//            ]
//        ]
//        razorpay.open(options)
//    }
//    public func onPaymentError(_ code: Int32, description str: String){
//        let alertController = UIAlertController(title: "FAILURE", message: str, preferredStyle: UIAlertControllerStyle.alert)
//        let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
//        alertController.addAction(cancelAction)
//        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
//    }
//
//    public func onPaymentSuccess(_ payment_id: String){
//        let alertController = UIAlertController(title: "SUCCESS", message: "Payment Id \(payment_id)", preferredStyle: UIAlertControllerStyle.alert)
//        let cancelAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
//        alertController.addAction(cancelAction)
//        self.view.window?.rootViewController?.present(alertController, animated: true, completion: nil)
//    }
    


    func checkBookingSTatus(){
        if let userID = mAuth.currentUser?.uid {
            let dataRef = db.collection("Bookings").document(userID)
                registration = dataRef.collection("shift1").document(currentDate).addSnapshotListener({ (snapShotOne, errOne) in
                    let bookingstatus = snapShotOne?.get("bookingStarted") as! Bool
                    let completed = snapShotOne?.get("completed") as! Bool
                        if (completed==false){
                            //aSwitch1.setEnabled(true);
                                if(bookingstatus)
                                {
                                   /// aSwitch1.setChecked(true);
                                }
                                else {
                                    //aSwitch1.setChecked(false);
                                }

                        }
                        else
                        {
                            //aSwitch1.setEnabled(false);
                            //aSwitch1.setText("Session 1 Completed");
                        }


                })

        }
        
    }
    @objc func updateTable(){
        tableView.reloadData()
        SVProgressHUD.dismiss()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }


    @IBAction func logOutButton(_ sender: Any) {
        
        if Auth.auth().currentUser?.uid != nil {
            do {
                try Auth.auth().signOut()
                performSegue(withIdentifier: "BackToLogin", sender: nil)
            }
            catch{
                print(error.localizedDescription)
            }
        }
    }
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections[section]
    }
//    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        //let view = UIView(frame: CGRect(0, 0, tableView.frame.size.width, 38))
//        //let label = UILabel(frame: CGRect(5, 0, tableView.frame.size.width, 38))
//        //label.font = UIFont(name: "Avenir-Book", size: 25)
//        //label.text =  sections[section]
//       // view.addSubview(label)
//        //view.backgroundColor = #colorLiteral(red: 0.8931408525, green: 0.9290409088, blue: 0.9372299314, alpha: 1)
//        return view
//    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if s1Data.count == 0{
                SVProgressHUD.show()
            }
            return s1Data.count
        }
        else if section == 1{
            return s2Data.count
        }
        else if section == 2{
            return s3Data.count
        }
        else {
            return 1
        }
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45
    }
    @objc func switchChanged(_ sender : UISwitch!){
        db.collection("Bookings").document((Auth.auth().currentUser?.uid)!)
            .collection("shift\(sender.tag + 1)").document(currentDate).updateData(["bookingStarted":sender.isOn ? true : false])
        print("table row switch Changed \(sender.tag)")
        print("The switch is \(sender.isOn ? "ON" : "OFF")")
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! SettingsTableViewCell
            cell.CellTitle.text = s1Data[indexPath[1]]
            cell.selectionStyle = .none
            cell.switchButton.tag = indexPath.row // for detect which row switch Changed
            cell.switchButton.addTarget(self, action: #selector(self.switchChanged(_:)), for: .valueChanged)
            if let userID = mAuth.currentUser?.uid {
                let dataRef = db.collection("Bookings").document(userID)
                registration = dataRef.collection("shift\(indexPath.row + 1)").document(currentDate).addSnapshotListener({ (snapShotOne, errOne) in
                    if self.registration != nil {
                    let bookingstatus = snapShotOne?.get("bookingStarted") as! Bool
                    let completed = snapShotOne?.get("completed") as! Bool
                    if (completed==false){
                        cell.switchButton.isEnabled = true
                        cell.CellTitle.text = "\(self.s1Data[indexPath.row])"
                       cell.switchButton.isOn = true
                        if(bookingstatus)
                        {
                            cell.switchButton.isOn = true
                        }
                        else {
                            cell.switchButton.isOn = false
                        }
                    }
                    else
                    {
                       cell.switchButton.isOn = false
                        cell.switchButton.isEnabled = false
                        cell.CellTitle.text = "\(self.s1Data[indexPath.row]) Completed"
                    }
                }
                })
            }
             return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! SettingsTableViewCell
            cell.CellTitle.text = s2Data[indexPath[1]]
            if let userID = mAuth.currentUser?.uid {
                db.collection("DoctorCity").document(userID).getDocument(completion: { (documentSnapshot, err) in
                    if (documentSnapshot?.exists)!
                    {

                        self.city = documentSnapshot?.get("city") as! String
                        self.db.collection("Doctors").document(self.city).collection("DoctorsList").document(userID).addSnapshotListener({ (documentSnapshot, err) in
                            if (documentSnapshot?.exists)!
                            {
                                self.numberOfShift = documentSnapshot!.get("numberOfShift") as! Int32
                                cell.noOfPatient.text = String(documentSnapshot?.get("maxPatient\(indexPath.row + 1)") as! Int32)
                                cell.cell2TextField.tag = indexPath.row
                                cell.cell2TextField.addTarget(self, action: #selector(self.textFieldDidChange(textField:)), for: UIControlEvents.editingDidEnd)
                                //faq, stats

                            }
                        })


                    }
                })
            }
            cell.selectionStyle = .none
            return cell
        }
        else if indexPath.section == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! SettingsTableViewCell
            cell.CellTitle.text = s3Data[indexPath[1]]
            cell.cell4Button.tag = indexPath.row
            cell.cell4Button.addTarget(self, action: #selector(self.performSegueCell(sender:)), for: .touchUpInside)
            if indexPath.row == 2{
                cell.cell4Button.addTarget(self, action: #selector(self.callOFF(sender:)), for: .touchUpInside)
            }
            else if indexPath.row == 3 {
                cell.cell4Button.addTarget(self, action: #selector(referADoctor(sender:)), for: .touchUpInside)
            }
            else if indexPath.row == 4{
                 cell.cell4Button.addTarget(self, action: #selector(resetPassword(sender:)), for: .touchUpInside)
            }
            else if indexPath.row == 5 {
                 cell.cell4Button.addTarget(self, action: #selector(contactUS(sender:)), for: .touchUpInside)
            }
            cell.selectionStyle = .none
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell4") as! SettingsTableViewCell
            cell.CellTitle.text = "Payments"
            if let userID = mAuth.currentUser?.uid {

                db.collection("DoctorCity").document(userID).getDocument(completion: { (documentSnapshot, err) in
                    if (documentSnapshot?.exists)!
                    { self.city = documentSnapshot?.get("city") as! String}
                    self.db.collection("Doctors").document(self.city).collection("DoctorsList").document(userID).addSnapshotListener({ (documentSnapshot, err) in
                    if (documentSnapshot?.exists)!
                    {
                        let myBalance = documentSnapshot?.get("myBalance") as! Double
                        if (myBalance>=0)
                        {
                            cell.CellTitle.text = "Available Balance: ₹\(myBalance)"
                            cell.cell4Button.setTitleColor(#colorLiteral(red: 0.05490196078, green: 0.6078431373, blue: 0.5568627451, alpha: 1), for: .normal)
                            cell.cell4Button.setTitle("Add Balance", for: .normal)
                           // cell.cell4Button.addTarget(self, action: <#T##Selector#>, for: .touchUpInside)
                        }
                        else {
                            cell.CellTitle.text = "My dues: ₹\(abs(myBalance))"
                            cell.cell4Button.setTitleColor(#colorLiteral(red: 0.7865244289, green: 0.01542204763, blue: 0.01542204763, alpha: 1), for: .normal)
                            cell.cell4Button.setTitle("Clear Dues", for: .normal)
                        }
                    }
                })
                    })
            }

            cell.selectionStyle = .none
            return cell
        }
    }

    @objc func callOFF(sender: UIButton){
        let alert = UIAlertController(title: "Confirmation", message: "Are you sure to call off all your task of the day.?\n Note- You will not be able to reverse this action.", preferredStyle: .alert)
        //alert.addTextField {(textField1: UITextField!) -> Void in
         //   textField1.placeholder = "Enter amount"
         //   textField1.keyboardType = .numberPad
       // }

        let saveAction = UIAlertAction(title: "Call off",style: .default){
            (action: UIAlertAction!) -> Void in
            //let textField1 = alert.textFields![0] as UITextField

            if let userID =  Auth.auth().currentUser?.uid {
                self.db.collection("Doctors").document(self.city).collection("DoctorsList").document(userID).getDocument(completion: { (document, err) in
                    //let numb = document?.get("numberOfShift") as! Int32
                    var dict = Dictionary<String, Any>()
                    dict["queueStatus"] = "Due to an Emergency Doctor terminates all the task of the day."
                    dict["bookingStarted"] = false
                    dict["completed"] = true
                    var liveUpdate = Dictionary<String,Any>()
                    liveUpdate["timestamp"] = FieldValue.serverTimestamp();
                    liveUpdate["message"] = "Due to an Emergency Doctor terminates all the task of the day."
                    self.db.collection("Bookings").document((Auth.auth().currentUser?.uid)!).collection("shift1").document(self.currentDate).collection("liveUpdates").addDocument(data: liveUpdate, completion: { (err2) in
                        if err2 == nil{
                            SVProgressHUD.showSuccess(withStatus: "Status Saved")
                        }
                    })
                    for i in 1...self.numberOfShift{
                        let dbBooking = self.db.collection("Bookings").document(userID) .collection("shift\(i)").document(self.currentDate)
                        dbBooking.addSnapshotListener( { (documentSnapshot, error) in
                            let liveNumber = documentSnapshot?.get("liveNumber") as! Int32
                            let totalTokens = Int(documentSnapshot?.get("tokens") as! Int32)
                            if (self.sessionCompleted==true)
                            {

                            }
                            else if (self.sessionCompleted==false)
                            {
                                dbBooking.updateData(dict, completion: { (err) in
                                    if (liveNumber==0)
                                    {
                                        for j in 1...totalTokens{
                                            dbBooking.collection("appointments").whereField("tokens", isEqualTo: j).limit(to: 1).getDocuments(completion: { (docs, err3) in
                                                for document3 in (docs?.documents)!{
                                                    //let patientName = document3.get("patientName") as! String
                                                    //let patientNumber = document3.get("patientPNumber") as! String
                                                    let patientStatus = document3.get("patientStatus") as! String
                                                    if (patientStatus == ("Cancelled"))
                                                    { }
                                                    else
                                                    {
                                                        //sendSMStoCallOffUser(patientName,patientNumber);
                                                    }
                                                }
                                            })
                                        }

                                    }
                                    else {
                                        for y in stride(from: Int(liveNumber) + 1, through: totalTokens, by: 1){
                                            dbBooking.collection("appointments").whereField("tokens", isEqualTo: y).getDocuments(completion: { (doc4, err4) in
                                                for document4 in (doc4?.documents)!{
                                                    //_ = document4.get("patientName") as! String
                                                    //let patientNum = document4.get("patientPNumber") as! String
                                                    let
                                                    patientStatus = document4.get("patientStatus") as! String
                                                    if (patientStatus == "Cancelled")
                                                    { }
                                                    else
                                                    {
                                                        //sendSMStoCallOffUser(patientName,patientNumber);
                                                    }
                                                }
                                            })

                                        }
                                    }
                                })
                            }
                        })
                    }
                })
            }


            SVProgressHUD.showSuccess(withStatus: "Done")
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default) { (action: UIAlertAction!) -> Void in}


        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert,animated: true,completion: nil)
    }
    @objc func resetPassword(sender: UIButton){
        let alert = UIAlertController(title: "Reset Password", message:"Please enter the email address you signed up with and we'll send you a password reset link.", preferredStyle: .alert)
        alert.addTextField {(textField1: UITextField!) -> Void in
           textField1.placeholder = "Enter Email"
           textField1.keyboardType = .emailAddress
         }

        let saveAction = UIAlertAction(title: "Reset?",style: .default){
            (action: UIAlertAction!) -> Void in
            let emailTextField = alert.textFields![0] as UITextField
            if emailTextField.text == "" {
                let alertController = UIAlertController(title: "Oops!", message: "Please enter an email.", preferredStyle: .alert)

                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)

                self.present(alertController, animated: true, completion: nil)

            } else {
                Auth.auth().sendPasswordReset(withEmail: emailTextField.text!, completion: { (error) in

                    var title = ""
                    var message = ""

                    if error != nil {
                        title = "Error!"
                        message = (error?.localizedDescription)!
                    } else {
                        title = "Success!"
                        message = "Password reset email sent."
                        emailTextField.text = ""
                    }

                    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)

                    self.present(alertController, animated: true, completion: nil)
                })
            }
            SVProgressHUD.showSuccess(withStatus: "Check your email")
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default) { (action: UIAlertAction!) -> Void in}


        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert,animated: true,completion: nil)
    }
    @objc func contactUS(sender: UIButton){
        let alert = UIAlertController(title: "Help", message:"You can also drop us mail at help@cleaniq.in.", preferredStyle: .alert)
        alert.addTextField {(textField1: UITextField!) -> Void in
            textField1.placeholder = "Enter Message"
        }

        let saveAction = UIAlertAction(title: "Send?",style: .default){
            (action: UIAlertAction!) -> Void in
            let msg = alert.textFields![0].text
            var map = Dictionary<String, Any>()
            map["reason"] = msg
            map["timestamp"] = FieldValue.serverTimestamp()
            map["status"] = "Received"
            map["doctorKey"] = Auth.auth().currentUser?.uid
            self.db.collection("Help").addDocument(data: map).addSnapshotListener({ (_, err5) in
                if err5 != nil{

                }
                else{
                    let alert = UIAlertController(title: "Done", message:"Your request has been submitted. We will get in touch with you as soon as possible.", preferredStyle: .alert)

                    let saveAction = UIAlertAction(title: "Ok",style: .default){
                        (action: UIAlertAction!) -> Void in
                        SVProgressHUD.showSuccess(withStatus: "Sent")
                    }
                    alert.addAction(saveAction)
                    self.present(alert,animated: true,completion: nil)
                }
            })
            SVProgressHUD.showSuccess(withStatus: "Sent")
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default) { (action: UIAlertAction!) -> Void in}


        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert,animated: true,completion: nil)
    }
    @objc func referADoctor(sender: UIButton){
        let alert = UIAlertController(title: "Refer a Doctor", message: "We will contact him/her on behalf of your reference. ", preferredStyle: .alert)
        alert.addTextField {(textField: UITextField!) -> Void in
            textField.placeholder = "Doctor Name"
        }
        alert.addTextField {(textField1: UITextField!) -> Void in
          textField1.placeholder = "Doctor Number"
          textField1.keyboardType = .numberPad
         }

        let saveAction = UIAlertAction(title: "Invite",style: .default){
            (action: UIAlertAction!) -> Void in
            let DoctorName = alert.textFields![0].text
            let DoctorNumber = alert.textFields![1].text
            if DoctorName != nil && DoctorNumber != nil{
                var map = Dictionary<String, Any>()
                map["docotrName"] = DoctorName
                map["doctorPhone"] = DoctorNumber
                map["timestamp"] = FieldValue.serverTimestamp()
                map["doctorKey"] = Auth.auth().currentUser?.uid
                self.db.collection("Submits").addDocument(data: map).addSnapshotListener({ (_, err5) in
                    if err5 != nil{

                    }
                    else{
                        let alert = UIAlertController(title: "Done", message:"Your Invite has been submitted. We will get in touch with \(String(describing: DoctorName)) as soon as possible.", preferredStyle: .alert)

                        let saveAction = UIAlertAction(title: "Ok",style: .default){
                            (action: UIAlertAction!) -> Void in
                            SVProgressHUD.showSuccess(withStatus: "Sent")
                        }
                        alert.addAction(saveAction)
                        self.present(alert,animated: true,completion: nil)
                    }
                })
            }
            SVProgressHUD.showSuccess(withStatus: "Sent")
        }
        let cancelAction = UIAlertAction(title: "Cancel",style: .default) { (action: UIAlertAction!) -> Void in}


        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert,animated: true,completion: nil)
    }
    @objc func performSegueCell(sender: UIButton){
        if sender.tag == 0{
            //performSegue(withIdentifier: "toFAQ", sender: nil)
        }
        else if sender.tag == 1 {
            performSegue(withIdentifier: "toRecord", sender: nil)
        }

    }
    @objc func textFieldDidChange(textField: UITextField) {
        self.db.collection("Doctors").document(self.city).collection("DoctorsList").document((Auth.auth().currentUser?.uid)!).updateData(["maxPatient\(textField.tag + 1)" : Int(textField.text!) ?? 0])
    }
    func getTotalNumberOfShift(){
        if let userID = Auth.auth().currentUser?.uid {
            let docRef = db.collection("DoctorCity").document(userID)
            docRef.getDocument(completion: { (snapshot, err) in
                let city = snapshot?.get("city")
                self.db.collection("Doctors").document(city as! String).collection("DoctorsList").document(userID).getDocument(completion: { (docSnapshot, err) in
                    let numberOfShift = docSnapshot?.get("numberOfShift") as! Int
                    for number in 0..<(numberOfShift){
                        self.s1Data.append("Session \(number+1)")
                        self.s2Data.append("Total Patient in S\(number+1)")
                    }
                })
            })
        }
    }

}
extension CGRect{
    init(_ x:CGFloat,_ y:CGFloat,_ width:CGFloat,_ height:CGFloat) {
        self.init(x:x,y:y,width:width,height:height)
    }
}
extension CGSize{
    init(_ width:CGFloat,_ height:CGFloat) {
        self.init(width:width,height:height)
    }
}
extension CGPoint{
    init(_ x:CGFloat,_ y:CGFloat) {
        self.init(x:x,y:y)
    }
}
