//
//  AdminPanel0ViewController.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 25/08/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit
import FirebaseFirestore
import SVProgressHUD
import FirebaseAuth
class Panel: UIViewController,UITableViewDelegate, UITableViewDataSource {
    let db = Firestore.firestore()
    let tableView : UITableView = {
        let t = UITableView()
        t.translatesAutoresizingMaskIntoConstraints = false
        return t
    }()
    var totalNumberOfButtons = 0
    var shifts:[String] = ["Shift One", "Shift Two", "Shift Three", "Shift Four", "Shift Five"]
    //@IBOutlet var shiftButtons: [UIButton]!
    @IBOutlet weak var shiftViewLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.show()
         //backgroundView = UIImageView(image: UIImage(named: "plz"))
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(self.updateTable), userInfo: nil, repeats: false)
        let logo = UIImage(named: "cleaniqText.png")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        getTotalNumberOfShift()
        //tableView.separatorColor = view.backgroundColor
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(AdminPanelTableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.backgroundColor = .clear
        tableView.isScrollEnabled = false
        view.addSubview(tableView)
        setupTableView()

    }
    @objc func updateTable(){
        tableView.reloadData()
        SVProgressHUD.dismiss()
    }
    func getTotalNumberOfShift(){
        if let userID = Auth.auth().currentUser?.uid {
            let docRef = db.collection("DoctorCity").document(userID)
            docRef.getDocument(completion: { (snapshot, err) in
                let city = snapshot?.get("city")
                self.db.collection("Doctors").document(city as! String).collection("DoctorsList").document(userID).getDocument(completion: { (docSnapshot, err) in
                    let numberOfShift = docSnapshot?.get("numberOfShift") as! Int
                    self.totalNumberOfButtons =  self.shifts.count - numberOfShift
                })
            })
        }
    }
    func setupTableView(){
        tableView.heightAnchor.constraint(equalTo: view.heightAnchor, constant: shiftViewLabel.frame.height + 20).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor, constant: -40).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        tableView.topAnchor.constraint(equalTo: shiftViewLabel.bottomAnchor, constant: 20).isActive = true
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }



    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return totalNumberOfButtons - 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AdminPanelTableViewCell

        cell.contentView.backgroundColor = UIColor.clear
//        let whiteRoundedView : UIView = UIView(frame: CGRect(0, 10, self.view.frame.size.width, 70))
//        whiteRoundedView.layer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [1.0, 1.0, 1.0, 1.0])
//        whiteRoundedView.layer.masksToBounds = false
//        whiteRoundedView.layer.cornerRadius = 10.0
//        whiteRoundedView.layer.shadowOffset = CGSize(0, 1)
//        whiteRoundedView.layer.shadowOpacity = 0.5
//        cell.contentView.addSubview(whiteRoundedView)
//        cell.contentView.sendSubview(toBack: whiteRoundedView)
        cell.selectionStyle = .none
        cell.textLabel?.font = UIFont(name: "Avenir", size: 20)
        cell.textLabel?.text = shifts[indexPath.row]
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       print(indexPath.row)
        performSegue(withIdentifier: "toAdminPanel2", sender: nil)
    }

}
