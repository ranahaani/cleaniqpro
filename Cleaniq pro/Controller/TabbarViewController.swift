//
//  UITabBarController.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 23/08/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit

class TabbarViewController: UITabBarController {
    fileprivate lazy var defaultTabBarHeight = { tabBar.frame.size.height }()
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedIndex = 2
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        let newTabBarHeight = defaultTabBarHeight + 16.0

        var newFrame = tabBar.frame
        newFrame.size.height = newTabBarHeight
        newFrame.origin.y = view.frame.size.height - newTabBarHeight

        tabBar.frame = newFrame
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
