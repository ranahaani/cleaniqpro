//
//  PatientListViewController.swift
//  Cleaniq pro
//
//  Created by Muhammad Abdullah on 20/09/2018.
//  Copyright © 2018 Muhammad Abdullah. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseFirestore
import SVProgressHUD
class PatientListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let currentDate: String = {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let Finaldate = dateFormatter.string(from: date)
        return Finaldate
    }()
    var registration:ListenerRegistration!
    var PName = [String]()
    var Page = [String]()
    var db = Firestore.firestore()
    var Pphone = [String]()
    var Ptoken = [Int]()
    var Pfee = [Int32]()
    var PStatus = [String]()
    var Ptime = [Date]()
    var patientGender = [String]()
    var pDocId = [String]()
    var isLoaded = true
    var shiftNumber:Int32!
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        SVProgressHUD.show(withStatus: "Please Wait...")
        super.viewDidLoad()
        PName.append("Name")
        Ptoken.append(0)
        PStatus.append("booked")
        Pphone.append("phome")
        Page.append("Age")
        patientGender.append("Male")
        print(shiftNumber)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        //refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
        let logo = UIImage(named: "cleaniqText.png")
        let imageView = UIImageView(image:logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.wait5Sec), userInfo: nil, repeats: false)
        // Do any additional setup after loading the view.
    }

//    @objc func refresh() {
//        tableView.reloadData()
//        refreshControl.endRefreshing()
//    }
    @objc func wait5Sec(){
        SVProgressHUD.dismiss()
        tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let userId = Auth.auth().currentUser?.uid{
            if shiftNumber != nil{
            registration = db.collection("Bookings").document(userId).collection("shift\(Int(shiftNumber))").document(currentDate).collection("appointments").addSnapshotListener({ (querySnapshot, err) in
                if self.Page[0].first == "A"{
                self.Page.removeAll()
                self.Pphone.removeAll()
                self.PName.removeAll()
                self.Ptoken.removeAll()
                self.PStatus.removeAll()
                }
                if let err = err {
                    print("Error getting documents: \(err)")
                } else {
                    for document in querySnapshot!.documents {
                        let age = "Age -"
                        self.Page.append(contentsOf: [age + (document.get("patientAge") as! String)])
                        self.PName.append(contentsOf:[document.get("patientName") as! String])
                        self.Ptoken.append(contentsOf:[document.get("tokens") as! Int])
                        self.Pphone.append(contentsOf: [document.get("patientPNumber") as! String])
                        self.Pfee.append(contentsOf: [document.get("patientFee") as! Int32])
                         self.PStatus.append(contentsOf: [document.get("patientStatus") as! String])
                        if let timestamp: Timestamp = document.get("joindate") as? Timestamp{
                            let JoinDate: Date = timestamp.dateValue()
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "dd-MM-yyyy"
                            self.Ptime.append(JoinDate)
                        }
                        self.pDocId.append(document.documentID)
                        self.patientGender.append(contentsOf: [document.get("patientGender") as! String])
                        //self.Ptoken.append(contentsOf: [document.get("tokens") as! Int])
                    }
                }

            })
            } 
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if registration != nil{
            registration.remove()
        }
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let edit = editText(indexPath: indexPath)
        let delete = deleteText(indexPath: indexPath)
        let message = messageAction(indexPath: indexPath)
        return UISwipeActionsConfiguration(actions: [edit, delete, message])
    }
    func messageAction(indexPath: IndexPath)->UIContextualAction{
        let message = UIContextualAction(style: .normal, title: "Message") { (action, view, completion) in
            let alert = UIAlertController(title: "Edit Patient", message: "", preferredStyle: .alert)

            let ConfirmationAction = UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                self.PName[indexPath.row] = alert.textFields![0].text!


                if (self.pDocId[indexPath.row].count==10)
                {
                    //sendSMStoUser(text,model.getPatientPNumber(),model.getPatientName());
                }
                else
                {
                    self.sendPushtoUser(text: alert.textFields![0].text!,id: self.pDocId[indexPath.row])
                }
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (_) in })
            alert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Enter Message"
                let heightConstraint = NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100)
                textField.addConstraint(heightConstraint)
            })

            alert.addAction(ConfirmationAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            completion(true)

        }
        //message.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        message.image = #imageLiteral(resourceName: "message")

        return message
    }
    func sendPushtoUser(text: String, id:String){
        var map = Dictionary<String, Any>()
        db.collection("Patients").document(id).getDocument { (documentSnapshot, err) in
            let device_token = documentSnapshot?.get("device_token") as! String
            let patient_name = documentSnapshot?.get("patientName") as! String
            map["device_token"] = device_token
            map["patientName"] = patient_name
            map["message"] = text
            self.db.collection("Notifications").addDocument(data: map).addSnapshotListener({ (s, err) in
                if err != nil{
                    SVProgressHUD.showError(withStatus: "Message Failed")
                }
                else{
                    SVProgressHUD.showSuccess(withStatus: "Message Send")
                }
            })
        }
    }
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        let PatientInfo = PatientInfoAction(indexPath: indexPath)
        let markLate = markLateAction(indexPath: indexPath)
        return UISwipeActionsConfiguration(actions: [PatientInfo, markLate])
    }
    func markLateAction(indexPath: IndexPath) -> UIContextualAction{
        let markLate = UIContextualAction(style: .normal, title: "Mark Late") { (action, view, completion) in
            let alert = UIAlertController(title: "", message: "Want to Mark Late this?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .default, handler: { (_) in
                print("No")
            })
            let saveaction = UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                if (self.PStatus[indexPath.row].caseInsensitiveCompare("Completed")) == (ComparisonResult.orderedSame) || (self.PStatus[indexPath.row].caseInsensitiveCompare("Canceled")) == (ComparisonResult.orderedSame)
                {
                    SVProgressHUD.showInfo(withStatus: "Not Cancelable")
                }
                else{
                    var mdata = Dictionary<String , Any>()
                    mdata["patientStatus"] = "Late"
                    self.db.collection("Bookings").document((Auth.auth().currentUser?.uid)!).collection("shift\(Int(self.shiftNumber))").document(self.currentDate)
                        .collection("appointments").document(self.pDocId[indexPath.row]).updateData(mdata)
                    if (self.pDocId[indexPath.row].count==10)
                    {

                    }
                    else
                    {
                        self.incrementChance(docId: self.pDocId[indexPath.row])
                    }
                    SVProgressHUD.showSuccess(withStatus: "Done")
                }
                completion(true)
            })
            alert.addAction(cancelAction)
            alert.addAction(saveaction)
            self.present(alert, animated: true, completion: nil)
        }
        markLate.image = #imageLiteral(resourceName: "icons8-checkmark-25")
        return markLate
    }
    func incrementChance(docId: String){
        var userChances:Int32 = 0
        let docRefIncrement:DocumentReference  = db.collection("Patients").document(docId)
        self.db.runTransaction({ (transaction, errorPointer) -> Any? in
            let sfDocument: DocumentSnapshot
            do {
                try sfDocument = transaction.getDocument(docRefIncrement)
                 userChances = sfDocument.get("userChances") as! Int32
                userChances += 1
            } catch let fetchError as NSError {
                errorPointer?.pointee = fetchError
                return nil
            }
            transaction.updateData(["userChances": userChances], forDocument: docRefIncrement)
            return nil
        }) { (object, error) in
            if let error = error {
                print("\(error)")
            } else {
                print("Transaction successfully committed!")
            }
        }
        
    }
    func PatientInfoAction(indexPath: IndexPath)->UIContextualAction{
        let PatientInfo = UIContextualAction(style: .destructive, title: "Patient Info") { (action, view, completion) in
            let alert = UIAlertController(title: "patient Info", message: "", preferredStyle: .alert)
            let action = UIAlertAction(title: "Got it", style: .default, handler: { (_) in})
            //action.setValue(UIColor.blue, forKey: "Got it")
            let height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: nil, attribute: NSLayoutAttribute.notAnAttribute, multiplier: 1, constant: self.view.frame.height * 0.80)

            let NameLabel = UILabel(frame: CGRect(x: 20, y: 30, width: 150, height: 50))
            NameLabel.text = "Name - \(self.PName[indexPath.row])"
            let AgeLabel = UILabel(frame: CGRect(x: 20, y: 80, width: 150, height: 50))
            AgeLabel.text = "\(self.Page[indexPath.row])"
            let phoneLabel = UILabel(frame: CGRect(x: 20, y: 130, width: 180, height: 50))
            phoneLabel.text = "Phone - \(self.Pphone[indexPath.row])"
            let patientStatus = UILabel(frame: CGRect(x: 20, y: 180, width: 150, height: 50))
            patientStatus.text = "Status - \(self.PStatus[indexPath.row])"
            let token = UILabel(frame: CGRect(x: 160, y: 230, width: 120, height: 50))
            token.text = "Token - \(self.Ptoken[indexPath.row])"
            let PatientFee = UILabel(frame: CGRect(x: 20, y: 230, width: 100, height: 50))
            PatientFee.text = "Fee Paid - \(self.Pfee[indexPath.row])"
            alert.view.addConstraint(height)
            alert.view.addSubview(NameLabel)
            alert.view.addSubview(PatientFee)
            alert.view.addSubview(token)
            alert.view.addSubview(patientStatus)
            alert.view.addSubview(AgeLabel)
            alert.view.addSubview(phoneLabel)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            //completion(true)
        }
        PatientInfo.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        PatientInfo.image = #imageLiteral(resourceName: "info")
        return PatientInfo
    }
    func deleteText(indexPath: IndexPath)->UIContextualAction{
        let delete = UIContextualAction(style: .normal, title: "Cancel") { (action,view, completion) in
            let alert = UIAlertController(title: "", message: "Want to Cancel this?", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "No", style: .default, handler: { (_) in
                print("No")
            })
            let saveaction = UIAlertAction(title: "Yes", style: .default, handler: { (_) in
                if (self.PStatus[indexPath.row].caseInsensitiveCompare("Completed")) == (ComparisonResult.orderedSame) || (self.PStatus[indexPath.row].caseInsensitiveCompare("Canceled")) == (ComparisonResult.orderedSame)
                {
                    SVProgressHUD.showInfo(withStatus: "Not Cancelable")
                }
                else{
                    var mdata = Dictionary<String , Any>()
                    mdata["patientStatus"] = "Cancelled"


                    self.db.collection("Bookings").document((Auth.auth().currentUser?.uid)!).collection("shift\(Int(self.shiftNumber))").document(self.currentDate)
                    .collection("appointments").document(self.pDocId[indexPath.row]).updateData(mdata)
                    SVProgressHUD.showSuccess(withStatus: "Canceled")
                }
                completion(true)
            })
            alert.addAction(cancelAction)
            alert.addAction(saveaction)
            self.present(alert, animated: true, completion: nil)
        }
        delete.image = #imageLiteral(resourceName: "cancel")
        delete.backgroundColor = UIColor.red

        return delete
    }

    func editText(indexPath: IndexPath)->UIContextualAction{
        let edit = UIContextualAction(style: .normal, title: "Edit") { (action, view, completion) in
            let alert = UIAlertController(title: "Edit Patient", message: "", preferredStyle: .alert)

            let ConfirmationAction = UIAlertAction(title: "Ok", style: .default, handler: { (_) in
                self.PName[indexPath.row] = alert.textFields![0].text!
                self.Pphone[indexPath.row] = alert.textFields![1].text!
                // 2 is for staus Status
                self.Page[indexPath.row] = alert.textFields![2].text!
                self.tableView.reloadRows(at: [indexPath], with: .automatic)


                var mData = Dictionary<String, Any>()
                mData["patientName"] = alert.textFields![0].text
                mData["patientPNumber"] = alert.textFields![1].text
                mData["patientAge"] = alert.textFields![2].text
                self.db.collection("Bookings").document((Auth.auth().currentUser?.uid)!)
                    .collection("shift\(Int(self.shiftNumber))").document(self.currentDate)
                    .collection("appointments").document(self.pDocId[indexPath.row])
                    .updateData(mData)
                SVProgressHUD.showSuccess(withStatus: "Updated")
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { (_) in })
            alert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Edit Name"
            })
            alert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Edit Phone Numer"
            })
            alert.addTextField(configurationHandler: { (textField) in
                textField.placeholder = "Edit Age"
            })
            alert.addAction(ConfirmationAction)
            alert.addChildViewController(HomeViewController())
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
            completion(true)

        }
        // edit.image = #imageLiteral(resourceName: "edit-draw-pencil-1")
        edit.backgroundColor = UIColor.blue
        edit.image = #imageLiteral(resourceName: "edit-draw-pencil-1")
        return edit
    }





    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! patientListTableViewCell
        cell.customerAge.text = Pphone[indexPath.row] //Phone (Ghalt kam)
        cell.customerPhone.text = PStatus[indexPath.row] //same ghalt Status
        cell.Name.text = PName[indexPath.row]
        if PStatus[indexPath.row] == "Processing"{
            cell.customerPhone.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        }
        cell.tokenNumber.text = String(Ptoken[indexPath.row])
         if self.Page[0].first == "A"{
        if (patientGender[indexPath.row].caseInsensitiveCompare("Male")) == (ComparisonResult.orderedSame) {
            cell.customImageView.image = #imageLiteral(resourceName: "boy")
        }
        else{
            cell.customImageView.image = #imageLiteral(resourceName: "girl")
            }
        }
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return PName.count
    }

}
